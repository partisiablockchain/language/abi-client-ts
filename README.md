# The TypeScript ABI client

## Common testing framework with Java
This library gets its tests genereated by abi-client library.
Read more [here](https://gitlab.com/partisiablockchain/language/abi-client/-/blob/main/src/test/java/com/partisiablockchain/language/abiclient/commontests/README.md).

## Setup

Requires: `tsc`, `npm`

Install dependencies with: `npm i`

To build: `npm run build`

To test : `npm run test`