/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("readSet", () => {
  const transactionReader = new TransactionReader(
    Buffer.from([1, 2, 3, 4]),
    TestingHelper.getContractAbiFromFile("contract_set.abi")
  );
  const setTypeSpec = TestingHelper.setTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u8));
  expect(() => transactionReader.readGeneric(setTypeSpec)).toThrowError(
    "Type Set is not supported in rpc"
  );
});

test("readMap", () => {
  const transactionReader = new TransactionReader(
    new BigEndianByteInput(Buffer.from([1, 2, 3, 4])),
    TestingHelper.getContractAbiFromFile("contract_simple_map.abi")
  );
  const mapTypeSpec = TestingHelper.mapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.Address),
    TestingHelper.simpleTypeSpec(TypeIndex.u8)
  );
  expect(() => transactionReader.readGeneric(mapTypeSpec)).toThrowError(
    "Type Map is not supported in rpc"
  );
});

test("readAvlTreeMap", () => {
  const transactionReader = new TransactionReader(
    new BigEndianByteInput(Buffer.from([1, 2, 3, 4])),
    TestingHelper.getContractAbiFromFile("contract_simple_map.abi")
  );
  const mapTypeSpec = TestingHelper.avlTreeMapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.Address),
    TestingHelper.simpleTypeSpec(TypeIndex.u8)
  );
  expect(() => transactionReader.readGeneric(mapTypeSpec)).toThrowError(
    "Type AvlTreeMap is not supported in rpc"
  );
});

test("testReadAllBytes", () => {
  // Payload from
  // https://reader.partisiablockchain.com/shards/Shard1/blockchain/transaction/3a7b48fc2cc9d2ea79fbb29819d68ca0b16effb070f11d00964f67ec8b7e05cf?requireFinal=false
  const validTransactionPayload = bytesFromBase64(
    "Ad3YrH4KCiTDxvHXrKltp9uRYIycRRzgHnxB9liarF4RfTiQMQ9ptLfsFLViZd9rywZ3+yKYJ7G2puAYj" +
      "pnTVhUAAAAAAAA8PAAAAYMho9FYAAAAAAAAAAAE/hfRAJNyyO06xbeQsy40k1nCx+kAAADIAwEAAAAGU" +
      "2hhcmQxAAAAAAAMGQwAWQBXAAAARABXAFcAAAA/ABcAVwBWAAAAAABXAFcAVwBPAEYAPgBPACIAAAAAA" +
      "DwAAAAAAAAAQwBXABoAVwBAAFAAVwAVAFcAVwBUAEsAJwAAAFcAFwBJAAAAVwBXAFQAVgAeAFYAPwAAA" +
      "FcAVwBXABYAVQBVADsACABXAD8AVwAAAFMARwBXAFcAVQBXAAAAVwBXACQACQAZAFEAVwBUAEQAAwBVA" +
      "FcAQQBXAAAAVgBXAEU="
  );
  const tooLongTransactionPayload = Buffer.alloc(validTransactionPayload.length + 10);
  arraycopy(
    validTransactionPayload,
    0,
    tooLongTransactionPayload,
    0,
    validTransactionPayload.length
  );
  expect(() =>
    TransactionReader.deserializeTransactionPayload(tooLongTransactionPayload)
  ).toThrowError("The input wasn't fully read, 10 remaining bytes.");

  // Payload from
  // https://node1.testnet.partisiablockchain.com/shards/Shard2/blockchain/transaction/1109b57d07a8a6e96b231dbcf33c301b364d452ad2b1dabba4e8191839d5a9dc?requireFinal=false
  const validEventPayload = bytesFromBase64(
    "AJxumr8xIrjBEKFmjLmU4rqZaZriTgDAgcfD+9WPmUkGAgIAAAApQ0hFQ0tfRVhJU1RFTkNFX0ZPUl9DT" +
      "05UUkFDVF9PUEVOX0FDQ09VTlQBAAAABHRydWUBAAAABlNoYXJkMgAAAAAAAAADAAAAAAAAAAEAAAAAA" +
      "AAAAgIA"
  );
  const tooLongEventPayload = Buffer.alloc(validEventPayload.length + 10);
  arraycopy(validEventPayload, 0, tooLongEventPayload, 0, validEventPayload.length);
  expect(() => TransactionReader.deserializeEventPayload(tooLongEventPayload)).toThrowError(
    "The input wasn't fully read, 10 remaining bytes."
  );
});

test("deserializationOfTransactionAndEvents", () => {
  const transactions: ExecutedTransaction[] = TestingHelper.loadExecutedTransactions();

  const namedTypesFound: Set<string> = new Set();

  for (const transaction of transactions) {
    const deserialized = deserializeExecutedTransaction(transaction);
    addAllNamedTypes(deserialized, namedTypesFound);
  }

  // Did not encounter
  // InnerEvent::Sync,
  // InnerEvent::Callback,
  // InnerSystemEvent::CreateAccount,
  // InnerSystemEvent::RemoveContract,
  // InnerSystemEvent::CreateShard,
  // InnerSystemEvent::RemoveShard,
  // on testnet

  const expectedNamedTypesFound: Set<string> = new Set(
    list(
      "Account",
      "InteractContract",
      "UpdateGlobalPluginState",
      "CheckExistence",
      "UpgradeSystemContract",
      "InnerTransaction",
      "CallbackEvent",
      "EventTransaction",
      "System",
      "UpdatePlugin",
      "CoreTransactionPart",
      "UpdateGlobalPluginStateEvent",
      "SetFeatureEvent",
      "UpdatePluginEvent",
      "ReturnEnvelope",
      "UpdateContextFreePluginState",
      "UpgradeSystemContractEvent",
      "SignedTransaction",
      "GlobalPluginStateUpdate",
      "SetFeature",
      "InteractWithContractTransaction",
      "CreateContractTransaction",
      "Consensus",
      "InnerPart",
      "Callback",
      "Transaction",
      "LocalPluginStateUpdate",
      "UpdateLocalPluginStateEvent",
      "DeployContract",
      "CheckExistenceEvent",
      "ExecutableEvent",
      "ShardRoute",
      "UpdateLocalPluginState"
    )
  );
  expect(namedTypesFound).toEqual(expectedNamedTypesFound);
});

const deserializeExecutedTransaction = (executedTransaction: ExecutedTransaction): ScValue => {
  if (executedTransaction.isEvent) {
    return TransactionReader.deserializeEventPayload(
      TestingHelper.transactionPayloadFromExecutedTransaction(executedTransaction)
    );
  } else {
    return TransactionReader.deserializeTransactionPayload(
      TestingHelper.transactionPayloadFromExecutedTransaction(executedTransaction)
    );
  }
};

const addAllNamedTypes = (value: ScValue, namedTypesFound: Set<string>) => {
  if (value instanceof ScValueEnum) {
    const scValueEnum = value as ScValueEnum;
    addAllNamedTypes(scValueEnum.item, namedTypesFound);
  } else if (value instanceof ScValueStruct) {
    const scValueStruct = value as ScValueStruct;
    namedTypesFound.add(scValueStruct.name); // commontests-ignore-array

    for (const field of scValueStruct.fieldsMap.values()) {
      addAllNamedTypes(field, namedTypesFound);
    }
  }
};

test("contractAbi", () => {
  const abi = requireNonNull(TransactionReader.getContractAbi());
  const signedTransaction = abi.getNamedType("SignedTransaction");
  expect(signedTransaction).toBeDefined();
  const executableEvent = abi.getNamedType("ExecutableEvent");
  expect(executableEvent).toBeDefined();
});
