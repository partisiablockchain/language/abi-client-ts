/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("buildExecutableEvent", () => {
  const eventBuilder = TransactionBuilder.createEventBuilder();

  eventBuilder.addOption();

  const eventTransactionBuilder = eventBuilder.addStruct();
  eventTransactionBuilder.addHash(
    "740736d2ceae6405ab1d09351b05860d9c02228d953a4e0192e03a027f18ce90"
  );

  const callbackEvent = eventTransactionBuilder.addEnumVariant(2).addEnumVariant(6).addStruct();
  callbackEvent.addStruct().addAddress("04fe17d1009372c8ed3ac5b790b32e349359c2c7e9");
  callbackEvent.addHash("fd51a6a025d01467ebc8de0d76aa4e7054e4f42ca64aa7f7d3d48c92319d42f8");
  callbackEvent.addBool(true);
  callbackEvent.addVecU8(
    bytesFromHex(
      "00000000000000000000000000000000000000000000000000000000" +
        "00000000000000000000000000000000000000000000000000000000"
    )
  );

  const shardRouteBuilder = eventTransactionBuilder.addStruct();
  shardRouteBuilder.addOption().addString("Shard1");
  shardRouteBuilder.addI64(new BN("1"));

  eventTransactionBuilder.addI64(new BN("0"));
  eventTransactionBuilder.addI64(new BN("0"));
  eventTransactionBuilder.addU8(3);

  eventTransactionBuilder.addOption();

  const bytes = eventBuilder.getBytes();
  expect(bytes).toEqual(
    bytesFromBase64(
      "AHQHNtLOrmQFqx0JNRsFhg2cAiKNlTpOAZLgOgJ/GM6QAgYE/hfRAJNyyO06" +
        "xbeQsy40k1nCx+n9UaagJdAUZ+vI3g12qk5wVOT0LKZKp/fT1IySMZ1C+AEA" +
        "AAA4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
        "AAAAAAAAAAAAAAAAAAABAAAABlNoYXJkMQAAAAAAAAABAAAAAAAAAAAAAAAA" +
        "AAAAAAMA"
    )
  );
});

test("buildSignedTransaction", () => {
  const transactionBuilder = TransactionBuilder.createTransactionBuilder();

  transactionBuilder.addSignature(
    "011c78637143139a10f13a57de898d6265f2a6a8d7e22c71096ea4e485fb37460a" +
      "77ae89d51e2d2cc92232fb835486dd8c81ad65f8df1cfe54ba8c218b1dffd409"
  );

  const innerPartBuilder = transactionBuilder.addStruct();
  const coreBuilder = innerPartBuilder.addStruct();
  coreBuilder.addI64(1);
  coreBuilder.addI64(new BN("1661947817871"));
  coreBuilder.addI64(0);

  const interactBuilder = innerPartBuilder.addStruct();
  interactBuilder.addAddress("04203b77743ad0ca831df9430a6be515195733ad91");
  interactBuilder.addVecU8(bytesFromHex("04000000010f"));

  const bytes = transactionBuilder.getBytes();
  expect(bytes).toEqual(
    bytesFromBase64(
      "ARx4Y3FDE5oQ8TpX3omNYmXypqjX4ixxCW6k5IX7N0YKd66J1R4tLMkiMvuD" +
        "VIbdjIGtZfjfHP5Uuowhix3/1AkAAAAAAAAAAQAAAYLzzy+PAAAAAAAAAAAE" +
        "IDt3dDrQyoMd+UMKa+UVGVczrZEAAAAGBAAAAAEP"
    )
  );
});

test("getContractAbi", () => {
  const builder = TransactionBuilder.createTransactionBuilder();
  const abi = requireNonNull(builder.getContractAbi());
  const signedTransaction = abi.getNamedType("SignedTransaction");
  expect(signedTransaction).toBeDefined();
  // The not-null check is needed for type checking TS tests.
  if (signedTransaction != null) {
    expect(signedTransaction.name).toEqual("SignedTransaction");
  }
  const executableEvent = abi.getNamedType("ExecutableEvent");
  expect(executableEvent).toBeDefined();
  if (executableEvent != null) {
    expect(executableEvent.name).toEqual("ExecutableEvent");
  }
});

test("transactionBuilderErrorMessages", () => {
  const transactionBuilder = TransactionBuilder.createTransactionBuilder();
  expect(() => transactionBuilder.addBool(false)).toThrowError(
    "In /signature, Expected type Signature, but got bool"
  );

  transactionBuilder.addSignature(
    "011c78637143139a10f13a57de898d6265f2a6a8d7e22c71096ea4e485fb37460a" +
      "77ae89d51e2d2cc92232fb835486dd8c81ad65f8df1cfe54ba8c218b1dffd409"
  );

  const innerPartBuilder = transactionBuilder.addStruct();
  innerPartBuilder.addStruct();
  innerPartBuilder.addStruct();
  expect(() => transactionBuilder.addBool(false)).toThrowError(
    "In root, Cannot add more arguments than the struct has fields."
  );
});

test("eventBuilderErrorMessages", () => {
  const eventBuilder = TransactionBuilder.createEventBuilder();
  expect(() => eventBuilder.addBool(false)).toThrowError(
    "In /origin_shard, Expected type Option, but got bool"
  );

  eventBuilder.addOption();

  const eventTransactionBuilder = eventBuilder.addStruct();
  eventTransactionBuilder.addHash(
    "740736d2ceae6405ab1d09351b05860d9c02228d953a4e0192e03a027f18ce90"
  );

  const callbackEvent = eventTransactionBuilder.addEnumVariant(2).addEnumVariant(6).addStruct();
  callbackEvent.addStruct().addAddress("04fe17d1009372c8ed3ac5b790b32e349359c2c7e9");

  eventTransactionBuilder.addStruct();
  expect(() => eventBuilder.addBool(false)).toThrowError(
    "In root, Cannot add more arguments than the struct has fields."
  );
});
