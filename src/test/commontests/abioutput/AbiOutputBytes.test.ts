/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("writeAll", () => {
  const bytes = BigEndianByteOutput.serialize((out) => {
    const wrap = new AbiOutputBytes(out);
    wrap.writeBoolean(false);
    wrap.writeI8(1);
    wrap.writeI8(1);
    wrap.writeI16(2);
    wrap.writeI16(2);
    wrap.writeI32(3);
    wrap.writeI64(new BN("4"));
    wrap.writeU8(5);
    wrap.writeU8(5);
    wrap.writeU16(6);
    wrap.writeU16(6);
    wrap.writeU32(7);
    wrap.writeU64(new BN("8"));
    wrap.writeSignedBN(new BN("9"), 4);
    wrap.writeUnsignedBN(new BN("42"), 4);
    wrap.writeBytes(Buffer.from([11, 12, 13]));
    wrap.writeString("Hello");
  });

  const reader = new BigEndianByteInput(bytes);
  expect(reader.readBoolean()).toBeFalsy();
  expect(reader.readI8()).toEqual(1);
  expect(reader.readI8()).toEqual(1);
  expect(reader.readI16()).toEqual(2);
  expect(reader.readI16()).toEqual(2);
  expect(reader.readI32()).toEqual(3);
  expect(reader.readI64().eq(new BN("4"))).toBeTruthy();
  expect(reader.readU8()).toEqual(5);
  expect(reader.readU8()).toEqual(5);
  expect(reader.readU16()).toEqual(6);
  expect(reader.readU16()).toEqual(6);
  expect(reader.readU32()).toEqual(7);
  expect(reader.readU64().eq(new BN("8"))).toBeTruthy();
  expect(reader.readI32()).toEqual(9);
  expect(reader.readU32()).toEqual(42);
  expect(reader.readBytes(3)).toEqual(Buffer.from([11, 12, 13]));
  expect(reader.readString()).toEqual("Hello");
});
