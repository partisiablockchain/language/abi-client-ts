/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("illegalHeaderBytes", () => {
  // Header bytes encoding "PBCPBC" rather than "PBCABI"
  const parser = new AbiParser(bytesFromHex("50424350424301000001000001"));
  expect(() => parser.parseAbi()).toThrowError(
    "Malformed header bytes, expecting PBCABI but was PBCPBC"
  );
});

test("parseHeadersOnly", () => {
  const headerString = "PBCABI";
  const simpleVersionClient = new AbiVersion(1, 0, 0);
  const simpleVersionBinder = new AbiVersion(1, 0, 0);

  // Header bytes, binder version 1.0.0, client version 1.0.0, shortname length 4
  const parser = new AbiParser(bytesFromHex("50424341424901000001000004"));
  const header = parser.parseHeader();
  expect(header.header).toEqual(headerString);
  expect(header.versionClient).toEqual(simpleVersionClient);
  expect(header.versionBinder).toEqual(simpleVersionBinder);
});

test("contractBoolFromFile", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const parser = TestingHelper.loadAbiParserFromFile("contract_booleans.abi");
  const result = parser.parseAbi();

  const contract = result.contract;

  expect(result.format().shortnameLength).toEqual(4);
  const firstStruct = contract.namedTypes[0];
  expect(firstStruct.name).toEqual("ExampleContractState");
  ParserHelper.assertFieldType(
    firstStruct,
    0,
    "my_bool",
    TestingHelper.simpleTypeSpec(TypeIndex.bool)
  );

  const init = requireNonNull(contract.init());
  expect(init.name).toEqual("initialize");
  ParserHelper.assertActionType(init, 0, "my_bool", TestingHelper.simpleTypeSpec(TypeIndex.bool));

  const action = requireNonNull(contract.getFunctionByName("update_my_bool"));
  expect(action.name).toEqual("update_my_bool");
  ParserHelper.assertActionType(action, 0, "value", TestingHelper.simpleTypeSpec(TypeIndex.bool));

  const stateStruct = contract.stateType as NamedTypeRef;
  expect(contract.getNamedType(stateStruct)).toEqual(firstStruct);
});

test("stringsContractTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-strings
  const parser = TestingHelper.loadAbiParserFromFile("contract_strings.abi");

  const headerString = "PBCABI";

  const result = parser.parseAbi();
  const contractAbi = result.contract;
  const structTypes = contractAbi.namedTypes;

  expect(result.header).toEqual(headerString);
  expect(structTypes.length).toEqual(1);

  const struct = structTypes[0] as StructTypeSpec;
  expect(struct.name).toEqual("ExampleContractState");
  expect(struct.fields).toHaveLength(1);
  ParserHelper.assertFieldType(
    struct,
    0,
    "my_string",
    TestingHelper.simpleTypeSpec(TypeIndex.String)
  );

  const init = requireNonNull(contractAbi.init());
  expect(init.name).toEqual("initialize");
  ParserHelper.assertActionType(
    init,
    0,
    "my_string",
    TestingHelper.simpleTypeSpec(TypeIndex.String)
  );

  const shortname = init.shortname;
  expect(shortname).toEqual(Buffer.alloc(4));

  // expect(contractAbi.getFunctionByShortname(Buffer.alloc(42))).toBeUndefined();

  const action = requireNonNull(contractAbi.getFunctionByName("update_my_string"));
  expect(action.name).toEqual("update_my_string");
  ParserHelper.assertActionType(action, 0, "value", TestingHelper.simpleTypeSpec(TypeIndex.String));

  const stateStruct = contractAbi.stateType as NamedTypeRef;
  expect(contractAbi.getNamedType(stateStruct)).toBe(structTypes[0]);
});

test("contractNumbersTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-numbers
  const parser = TestingHelper.loadAbiParserFromFile("contract_numbers.abi");
  const result = parser.parseAbi();

  const contractAbi = result.contract;
  expect(result.format().shortnameLength).toEqual(0x04);

  const structTypes = contractAbi.namedTypes;
  expect(structTypes).toHaveLength(1);

  const struct = structTypes[0] as StructTypeSpec;
  expect(struct.name).toEqual("ExampleContractState");

  expect(struct.fields).toHaveLength(8);
  ParserHelper.assertFieldType(struct, 0, "my_u8", TestingHelper.simpleTypeSpec(TypeIndex.u8));
  ParserHelper.assertFieldType(struct, 1, "my_u16", TestingHelper.simpleTypeSpec(TypeIndex.u16));
  ParserHelper.assertFieldType(struct, 2, "my_u32", TestingHelper.simpleTypeSpec(TypeIndex.u32));
  ParserHelper.assertFieldType(struct, 3, "my_u64", TestingHelper.simpleTypeSpec(TypeIndex.u64));
  ParserHelper.assertFieldType(struct, 4, "my_i8", TestingHelper.simpleTypeSpec(TypeIndex.i8));
  ParserHelper.assertFieldType(struct, 5, "my_i16", TestingHelper.simpleTypeSpec(TypeIndex.i16));
  ParserHelper.assertFieldType(struct, 6, "my_i32", TestingHelper.simpleTypeSpec(TypeIndex.i32));
  ParserHelper.assertFieldType(struct, 7, "my_i64", TestingHelper.simpleTypeSpec(TypeIndex.i64));

  const init = contractAbi.init();
  expect(requireNonNull(init).name).toEqual("initialize");

  const functions = contractAbi.functions;
  const actions = functions.slice(1, functions.length);
  expect(actions).toHaveLength(8); // 8 actions and init
  for (const action of actions) {
    expect(action.arguments).toHaveLength(1);
  }

  ParserHelper.assertActionType(actions[0], 0, "value", TestingHelper.simpleTypeSpec(TypeIndex.u8));
  ParserHelper.assertActionType(
    actions[1],
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.u16)
  );
  ParserHelper.assertActionType(
    actions[2],
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.u32)
  );
  ParserHelper.assertActionType(
    actions[3],
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );
  ParserHelper.assertActionType(actions[4], 0, "value", TestingHelper.simpleTypeSpec(TypeIndex.i8));
  ParserHelper.assertActionType(
    actions[5],
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.i16)
  );
  ParserHelper.assertActionType(
    actions[6],
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.i32)
  );
  ParserHelper.assertActionType(
    actions[7],
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.i64)
  );

  const stateStruct = contractAbi.stateType as NamedTypeRef;
  expect(contractAbi.getNamedType(stateStruct)).toBe(structTypes[0]);
});

test("simpleMapContractTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
  const parser = TestingHelper.loadAbiParserFromFile("contract_simple_map.abi");

  const result = parser.parseAbi();
  const contractAbi = result.contract;

  const struct = contractAbi.namedTypes[0] as StructTypeSpec;
  const structField = struct.fields[0];

  const actual = structField.type as MapTypeSpec;

  expect(actual).toEqual(
    TestingHelper.mapTypeSpec(
      TestingHelper.simpleTypeSpec(TypeIndex.Address),
      TestingHelper.simpleTypeSpec(TypeIndex.u8)
    )
  );

  expect(actual.keyType.typeIndex).toEqual(TypeIndex.Address);
  expect(actual.valueType.typeIndex).toEqual(TypeIndex.u8);

  const init = requireNonNull(contractAbi.init());
  expect(init.arguments.length).toEqual(0);

  const action = requireNonNull(contractAbi.getFunctionByName("insert_in_my_map"));

  expect(action.arguments.length).toEqual(2);
  expect(action.arguments[0].type.typeIndex).toEqual(TypeIndex.Address);
  expect(action.arguments[0].name).toEqual("address");
  expect(action.arguments[1].name).toEqual("value");

  const stateStruct = contractAbi.stateType as NamedTypeRef;

  expect(contractAbi.getNamedType(stateStruct)).toEqual(contractAbi.namedTypes[0]);
});

test("contract128BitInts", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-u128-and-i128
  const parser = TestingHelper.loadAbiParserFromFile("contract_u128_and_i128.abi");
  const result = parser.parseAbi();

  const struct = result.contract.namedTypes[0];
  ParserHelper.assertFieldType(struct, 0, "my_u128", TestingHelper.simpleTypeSpec(TypeIndex.u128));
  ParserHelper.assertFieldType(struct, 1, "my_i128", TestingHelper.simpleTypeSpec(TypeIndex.i128));
});

test("contractWithSetType", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-set
  const parser = TestingHelper.loadAbiParserFromFile("contract_set.abi");
  const result = parser.parseAbi();

  const expectedVersionBinder = new AbiVersion(1, 1, 0);
  const expectedVersionClient = new AbiVersion(2, 0, 0);

  expect(result.versionBinder).toEqual(expectedVersionBinder);
  expect(result.versionClient).toEqual(expectedVersionClient);

  const contractAbi = result.contract;
  const structTypeSpec = contractAbi.namedTypes[0] as StructTypeSpec;
  const setType = structTypeSpec.fields[0].type as SetTypeSpec;

  expect(setType).toEqual(TestingHelper.setTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64)));
  expect(setType.valueType.typeIndex).toEqual(TypeIndex.u64);
  expect(structTypeSpec.fields[0].name).toEqual("my_set");

  const action = requireNonNull(contractAbi.getFunctionByName("insert_in_my_set"));
  expect(action.arguments[0].type.typeIndex).toEqual(TypeIndex.u64);

  const stateStruct = contractAbi.stateType as NamedTypeRef;
  expect(contractAbi.getNamedType(stateStruct)).toEqual(contractAbi.namedTypes[0]);
});

test("simpleOptionContractTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-options
  const parser = TestingHelper.loadAbiParserFromFile("contract_options.abi");
  const result = parser.parseAbi();

  const contractAbi = result.contract;
  const struct = contractAbi.namedTypes[0] as StructTypeSpec;
  const fields = struct.fields;

  expect(fields).toHaveLength(5);

  ParserHelper.assertFieldType(
    struct,
    0,
    "option_u64",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
  );
  ParserHelper.assertFieldType(
    struct,
    1,
    "option_string",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.String))
  );
  ParserHelper.assertFieldType(
    struct,
    2,
    "option_address",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.Address))
  );
  ParserHelper.assertFieldType(
    struct,
    3,
    "option_boolean",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.bool))
  );
  ParserHelper.assertFieldType(
    struct,
    4,
    "option_map",
    TestingHelper.optionTypeSpec(
      TestingHelper.mapTypeSpec(
        TestingHelper.simpleTypeSpec(TypeIndex.u64),
        TestingHelper.simpleTypeSpec(TypeIndex.u64)
      )
    )
  );

  const init = requireNonNull(contractAbi.init());
  expect(init.arguments).toHaveLength(4);
  ParserHelper.assertActionType(
    init,
    0,
    "option_u64",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
  );
  ParserHelper.assertActionType(
    init,
    1,
    "option_string",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.String))
  );
  ParserHelper.assertActionType(
    init,
    2,
    "option_address",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.Address))
  );
  ParserHelper.assertActionType(
    init,
    3,
    "option_boolean",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.bool))
  );

  expect(contractAbi.functions.slice(1, contractAbi.functions.length)).toHaveLength(5);
});

test("simpleVecContract", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_simple_vector.abi");
  const result: FileAbi = parser.parseAbi();

  const contractAbi: ContractAbi = result.contract;
  const struct: StructTypeSpec = contractAbi.namedTypes[0] as StructTypeSpec;
  ParserHelper.assertFieldType(
    struct,
    0,
    "my_vec",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
  );

  ParserHelper.assertActionType(
    contractAbi.init(),
    0,
    "my_vec",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
  );
  ParserHelper.assertActionType(
    contractAbi.getFunctionByName("update_my_vec"),
    0,
    "value",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
  );
});

test("simpleArrayContract", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_byte_arrays.abi");
  const result: FileAbi = parser.parseAbi();

  const contractAbi: ContractAbi = result.contract;
  const structType: StructTypeSpec = contractAbi.namedTypes[0] as StructTypeSpec;

  const array16: TypeSpec = TestingHelper.sizedByteArrayTypeSpec(0x10);
  const array5: TypeSpec = TestingHelper.sizedByteArrayTypeSpec(0x05);

  ParserHelper.assertFieldType(structType, 0, "my_array", array16);
  ParserHelper.assertFieldType(structType, 1, "my_array_2", array5);

  ParserHelper.assertActionType(
    contractAbi.getFunctionByName("update_my_array"),
    0,
    "value",
    array16
  );
  ParserHelper.assertActionType(
    contractAbi.getFunctionByName("update_my_array_2"),
    0,
    "value",
    array5
  );
});

test("contractStructOfStructTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_struct_of_struct_v3.abi");
  const result: FileAbi = parser.parseAbi();

  const headerString = "PBCABI";

  expect(result.header).toEqual(headerString);
  const contract: ContractAbi = result.contract;
  expect(result.versionBinder).toEqual(new AbiVersion(3, 0, 0));
  expect(result.versionClient).toEqual(new AbiVersion(3, 0, 0));
  expect(contract).toBeDefined();

  const structTypes = contract.namedTypes;
  expect(structTypes).toHaveLength(3);

  const myStructType: StructTypeSpec = structTypes[0] as StructTypeSpec;
  expect(myStructType.name).toEqual("MyStructType");
  ParserHelper.assertFieldType(
    myStructType,
    0,
    "some_value",
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );
  ParserHelper.assertFieldType(
    myStructType,
    1,
    "some_vector",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
  );
  const initShortname = bytesFromHex("ffffffff0f");

  expect(requireNonNull(contract.init()).shortname).toEqual(initShortname);
  expect(
    requireNonNull(contract.getFunctionByName("update_my_other_struct")).shortname.toString("hex")
  ).toEqual("85f292be0b");
});

test("contractSimpleStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct/src
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_simple_struct_v3.abi");
  const result: FileAbi = parser.parseAbi();
  const contract: ContractAbi = result.contract;

  expect(result.versionBinder).toEqual(new AbiVersion(3, 0, 0));
  expect(result.versionClient).toEqual(new AbiVersion(3, 0, 0));

  const structTypes = contract.namedTypes;
  expect(structTypes).toHaveLength(3);
  const myStructType: StructTypeSpec = structTypes[0] as StructTypeSpec;
  ParserHelper.assertFieldType(
    myStructType,
    0,
    "some_value",
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );

  const nonStateStructType: StructTypeSpec = structTypes[1] as StructTypeSpec;
  ParserHelper.assertFieldType(
    nonStateStructType,
    0,
    "value",
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );

  const exampleContractState: StructTypeSpec = structTypes[2] as StructTypeSpec;
  ParserHelper.assertFieldType(exampleContractState, 1, "my_struct", TestingHelper.namedTypeRef(0));

  const updateMyu64UsingStruct: FnAbi = contract.functions[1];
  ParserHelper.assertActionType(updateMyu64UsingStruct, 0, "value", TestingHelper.namedTypeRef(1));

  const updateMyStruct: FnAbi = contract.functions[1];
  ParserHelper.assertActionType(updateMyStruct, 0, "value", TestingHelper.namedTypeRef(1));
});

test("contractCallback", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-callbacks/src
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_callbacks_v3.abi");
  const result: FileAbi = parser.parseAbi();
  expect(result.versionBinder).toEqual(new AbiVersion(3, 0, 0));
  expect(result.versionClient).toEqual(new AbiVersion(3, 0, 0));
  const contract: ContractAbi = result.contract;

  const exampleContractState: StructTypeSpec = contract.namedTypes[0] as StructTypeSpec;
  ParserHelper.assertFieldType(
    exampleContractState,
    0,
    "val",
    TestingHelper.simpleTypeSpec(TypeIndex.u32)
  );
  ParserHelper.assertFieldType(
    exampleContractState,
    1,
    "successful_callback",
    TestingHelper.simpleTypeSpec(TypeIndex.bool)
  );
  ParserHelper.assertFieldType(
    exampleContractState,
    2,
    "callback_results",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.bool))
  );
  ParserHelper.assertFieldType(
    exampleContractState,
    3,
    "callback_value",
    TestingHelper.simpleTypeSpec(TypeIndex.u32)
  );

  const actions = contract.functions;
  expect(actions).toHaveLength(5 + 1);
  const setAndCallbackFunction: FnAbi = actions[1];
  expect(setAndCallbackFunction.arguments).toHaveLength(1);
  expect(setAndCallbackFunction.shortname.toString("hex")).toEqual("01");
  // Ensure "callback" is not a callable action
  for (const a of actions) {
    expect(a.name).not.toEqual("callback");
  }
});

test("contractAllTypes", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-all-types
  const parser = TestingHelper.loadAbiParserFromFile("contract_all_types_v3.abi");
  const result = parser.parseAbi();
  expect(result.versionClient).toEqual(new AbiVersion(3, 0, 0));
  expect(result.versionBinder).toEqual(new AbiVersion(2, 1, 0));

  const contract: ContractAbi = result.contract;
  const structTypes = contract.namedTypes;
  expect(structTypes).toHaveLength(4);
  ParserHelper.assertFieldType(
    structTypes[1],
    1,
    "vector",
    TestingHelper.vecTypeSpec(TestingHelper.namedTypeRef(0))
  );

  const stateStruct: NamedTypeRef = contract.stateType as NamedTypeRef;
  expect(contract.getNamedType(stateStruct)).toEqual(structTypes[3]);

  const actions = contract.functions.slice(1, contract.functions.length);
  expect(actions).toHaveLength(23);
  expect(requireNonNull(contract.init()).arguments).toHaveLength(19);
  const updateMyOtherStruct: FnAbi = actions[22];
  expect(updateMyOtherStruct.arguments[0].type.typeIndex).toEqual(TypeIndex.Named);
  ParserHelper.assertActionType(updateMyOtherStruct, 0, "value", TestingHelper.namedTypeRef(0));
});

test("testContractStructOfWrongOrder", () => {
  // Contract can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/PAR-3123_recursive_types/contract-wrong-order-structs
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_wrong_order_structs.abi");
  const model: FileAbi = parser.parseAbi();
  const contract: ContractAbi = model.contract;
  const structTypes = contract.namedTypes;
  const state: NamedTypeRef = contract.stateType as NamedTypeRef;

  ParserHelper.assertFieldType(structTypes[0], 0, "child", TestingHelper.namedTypeRef(1));
  ParserHelper.assertFieldType(structTypes[state.index], 0, "value", TestingHelper.namedTypeRef(0));
});

test("testContractRecursiveTypes", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/PAR-3123_recursive_types/contract-recursive-types
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_recursive_types.abi");
  const model: FileAbi = parser.parseAbi();
  const structTypes = model.contract.namedTypes;
  const struct: StructTypeSpec = structTypes[0] as StructTypeSpec;

  const fieldType: VecTypeSpec = struct.fields[0].type as VecTypeSpec;
  const recursiveType: NamedTypeRef = fieldType.valueType as NamedTypeRef;
  expect(struct).toEqual(structTypes[recursiveType.index]);

  const nonSelfRecursiveStruct: StructTypeSpec = structTypes[1] as StructTypeSpec;
  const structType: NamedTypeRef = nonSelfRecursiveStruct.fields[0].type as NamedTypeRef;
  expect(structTypes[structType.index]).not.toEqual(nonSelfRecursiveStruct);
});

test("illegalVersionClient", () => {
  // Header bytes, binder version 3.0.0, client version 80.0.0
  const bytesMajor = bytesFromHex("504243414249030000500000");
  const parserMajor: AbiParser = new AbiParser(bytesMajor);

  expect(() => parserMajor.parseAbi()).toThrowError(
    "Unsupported Version 80.0.0 for Version Client."
  );

  // Header bytes, binder version 3.0.0, client version 1.66.0
  const bytesMinor = bytesFromHex("504243414249030000014200");
  const parserMinor: AbiParser = new AbiParser(bytesMinor);

  expect(() => parserMinor.parseAbi()).toThrowError(
    "Unsupported Version 1.66.0 for Version Client."
  );
});

test("testVersionCombinations", () => {
  ParserHelper.assertClientVersionFail(2, 1, 1, 66);
  ParserHelper.assertClientVersionFail(2, 0, 2, 66);
  ParserHelper.assertClientVersionFail(1, 0, 3, 2);
});

test("illegalByteAsType", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const bytes = TestingHelper.readBinaryFile("contract_booleans.abi");
  for (let i = 0; i < bytes.length; i++) {
    if (bytes[i] == 0x0c) {
      bytes[i] = 0x55; // Turn SimpleTypeSpec.bool into invalid type
    }
  }

  const parser: AbiParser = TestingHelper.abiParserFromBytes(bytes);

  expect(() => parser.parseAbi()).toThrowError("Bad byte 0x55 used for type");
});

test("illegalByteAsNamedType", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
  const bytes = TestingHelper.readBinaryFile("contract_enum.abi");
  // Overwrite the byte denoting that Vehicle is an enum (02)
  // only valid values is (02 - enum) and (01 - struct)
  bytes[16] = 0x55;

  const parser: AbiParser = TestingHelper.abiParserFromBytes(bytes);

  expect(() => parser.parseAbi()).toThrowError(
    "Bad byte 0x55 used for namedTypeSpec index should be either 0x01 for a struct or 0x02" +
      " for an enum"
  );
});

test("illegalByteAsEnumVariant", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
  const bytes = TestingHelper.readBinaryFile("contract_enum.abi");
  // Overwrite the byte denoting that the enum variant Bicycle is a named type
  // only valid values is (00 - NamedTypeRef)
  bytes[33] = 0x55;

  const parser: AbiParser = TestingHelper.abiParserFromBytes(bytes);

  expect(() => parser.parseAbi()).toThrowError(
    "Non named type Unknown used as an enum variant, each variant should be a reference to" +
      " a struct"
  );

  // Overwrite the byte denoting that the enum variant Bicycle is a named type
  // only valid values is (00 - NamedTypeRef)
  bytes[33] = 0x03;

  const parser2: AbiParser = TestingHelper.abiParserFromBytes(bytes);

  expect(() => parser2.parseAbi()).toThrowError(
    "Non named type u32 used as an enum variant, each variant should be a reference " +
      "to a struct"
  );
});

test("contractSecretInput", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi");
  const abi = parser.parseAbi();
  const onSecretInput = requireNonNull(abi.contract.getFunctionByName("swap"));
  expect(onSecretInput.kind).toEqual(FnKinds.zkSecretInputWithExplicitType);

  const secretArg = requireNonNull(onSecretInput.secretArgument);

  expect(secretArg.type.typeIndex).toEqual(TypeIndex.Named);

  const type = secretArg.type as NamedTypeRef;
  const secretStruct = abi.contract.getNamedType(type) as StructTypeSpec;
  expect(secretStruct.name).toEqual("AmountAndDirectionSecret");

  expect(secretStruct.fields[0].name).toEqual("amount");
  expect(secretStruct.fields[0].type.typeIndex).toEqual(TypeIndex.i128);
  expect(secretStruct.fields[1].name).toEqual("direction");
  expect(secretStruct.fields[1].type.typeIndex).toEqual(TypeIndex.i8);
});

test("contractSecretInputOldVersion", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi");
  const abi = parser.parseAbi().contract;
  const fn = requireNonNull(abi.getFunctionByName("add_vote"));
  expect(fn.kind).toEqual(FnKinds.zkSecretInput);
});

test("invalidLebEncodingInit", () => {
  const bytes = TestingHelper.readBinaryFile("contract_booleans_v3.abi");
  const initIndex = 70;
  bytes[initIndex + 4] = 0xff;

  const parser: AbiParser = new AbiParser(bytes);
  expect(() => parser.parseAbi()).toThrowError(
    "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int " +
      "(max 5 bytes)"
  );
});

test("shortnamesAreEncodedCorrectly", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_booleans_v3.abi");
  const result: FileAbi = parser.parseAbi();

  const initShortname = bytesFromHex("ffffffff0f");
  expect(requireNonNull(result.contract.init()).shortname).toEqual(initShortname);

  const expectedShortName = "b0a1fab30c";

  expect(
    requireNonNull(
      result.contract.getFunction(bytesFromHex(expectedShortName), FnKinds.action)
    ).shortname.toString("hex")
  ).toEqual(expectedShortName);
});

test("simpleTypesAbiVersion6AreParsedCorrectly", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-types-hash-to-bls-signature
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile(
    "contract_types_hash_to_bls_signature_v5.abi"
  );
  const result: FileAbi = parser.parseAbi();

  expect(
    requireNonNull(result.contract.getStateStruct().getFieldByName("u256")).type.typeIndex
  ).toEqual(TypeIndex.u256);
  expect(
    requireNonNull(result.contract.getStateStruct().getFieldByName("hash")).type.typeIndex
  ).toEqual(TypeIndex.Hash);
  expect(
    requireNonNull(result.contract.getStateStruct().getFieldByName("public_key")).type.typeIndex
  ).toEqual(TypeIndex.PublicKey);
  expect(
    requireNonNull(result.contract.getStateStruct().getFieldByName("signature")).type.typeIndex
  ).toEqual(TypeIndex.Signature);
  expect(
    requireNonNull(result.contract.getStateStruct().getFieldByName("bls_public_key")).type.typeIndex
  ).toEqual(TypeIndex.BlsPublicKey);
  expect(
    requireNonNull(result.contract.getStateStruct().getFieldByName("bls_signature")).type.typeIndex
  ).toEqual(TypeIndex.BlsSignature);

  expect(
    requireNonNull(result.contract.getFunctionByName("set_u256")).arguments[0].type.typeIndex
  ).toEqual(TypeIndex.u256);
  expect(
    requireNonNull(result.contract.getFunctionByName("set_hash")).arguments[0].type.typeIndex
  ).toEqual(TypeIndex.Hash);
  expect(
    requireNonNull(result.contract.getFunctionByName("set_public_key")).arguments[0].type.typeIndex
  ).toEqual(TypeIndex.PublicKey);
  expect(
    requireNonNull(result.contract.getFunctionByName("set_signature")).arguments[0].type.typeIndex
  ).toEqual(TypeIndex.Signature);
  expect(
    requireNonNull(result.contract.getFunctionByName("set_bls_public_key")).arguments[0].type
      .typeIndex
  ).toEqual(TypeIndex.BlsPublicKey);
  expect(
    requireNonNull(result.contract.getFunctionByName("set_bls_signature")).arguments[0].type
      .typeIndex
  ).toEqual(TypeIndex.BlsSignature);
});

test("abiFileTooLong", () => {
  const validAbi = TestingHelper.readBinaryFile("contract_booleans_v3.abi");
  const tooLongFile = concatBytes(validAbi, Buffer.alloc(1));
  const tooManyBytesParser: AbiParser = TestingHelper.abiParserFromBytes(tooLongFile);
  expect(() => tooManyBytesParser.parseAbi()).toThrowError(
    "Expected EOF after parsed ABI, but stream had 1 bytes remaining"
  );
});

test("unknownFnKind", () => {
  const bytes = bytesFromHex(
    "50" +
      "42" +
      "43" +
      "41" +
      "42" +
      "49" + // Header
      "04" +
      "00" +
      "00" + // Binder version 4.0.0
      "04" +
      "00" +
      "00" + // Client version 4.0.0
      "00" +
      "00" +
      "00" +
      "00" + // No Struct Types
      "00" +
      "00" +
      "00" +
      "01" + // A single hook
      "ff" + // Weird FnKind
      "00" +
      "00" +
      "00" +
      "00" + // fn name: ""
      "00" + // Shortname : 0
      "00" +
      "00" +
      "00" +
      "00" + // No arguments
      "01" // State type: u8
  );
  const parserMajor: AbiParser = TestingHelper.abiParserFromBytes(bytes);

  expect(() => parserMajor.parseAbi()).toThrowError("Unsupported FnKind type 0xff specified");
});

test("testContractAvlTreeMap", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/token-mapping
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("token_mapping.abi");
  const contract: ContractAbi = parser.parseAbi().contract;
  const state = contract.getStateStruct();
  expect(state.fields[0].name).toEqual("map");
  const type = state.fields[0].type;
  expect(type.typeIndex).toEqual(TypeIndex.AvlTreeMap);
  const avlType = type as AvlTreeMapTypeSpec;
  expect(avlType.keyType.typeIndex).toEqual(TypeIndex.Address);
  expect(avlType.valueType.typeIndex).toEqual(TypeIndex.u128);

  expect(state.fields[1].name).toEqual("result");
  expect(state.fields[1].type.typeIndex).toEqual(TypeIndex.Option);
});
