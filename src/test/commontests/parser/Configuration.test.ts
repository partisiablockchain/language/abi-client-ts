/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("clientConfigurationV100", () => {
  const version: AbiVersion = new AbiVersion(1, 0, 0);
  expect(Configuration.fromClientVersion(version, 0)).toBeDefined();
});

test("clientConfigurationV200", () => {
  const version: AbiVersion = new AbiVersion(2, 0, 0);
  expect(Configuration.fromClientVersion(version, 0)).toBeDefined();
});

test("clientConfigurationV300", () => {
  const version: AbiVersion = new AbiVersion(3, 0, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV310", () => {
  const version: AbiVersion = new AbiVersion(3, 1, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV400", () => {
  const version: AbiVersion = new AbiVersion(4, 0, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV410", () => {
  const version: AbiVersion = new AbiVersion(4, 1, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV500", () => {
  const version: AbiVersion = new AbiVersion(5, 0, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV510", () => {
  const version: AbiVersion = new AbiVersion(5, 1, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV520", () => {
  const version: AbiVersion = new AbiVersion(5, 2, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV530", () => {
  const version: AbiVersion = new AbiVersion(5, 3, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});

test("clientConfigurationV540", () => {
  const version: AbiVersion = new AbiVersion(5, 4, 0);
  expect(Configuration.fromClientVersion(version, 1)).toBeDefined();
});
