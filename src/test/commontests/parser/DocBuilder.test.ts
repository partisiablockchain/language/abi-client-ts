/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

const arg1: ArgumentAbi = TestingHelper.argumentAbi(
  "arg1",
  TestingHelper.simpleTypeSpec(TypeIndex.u64)
);
const arg2: ArgumentAbi = TestingHelper.argumentAbi(
  "arg2",
  TestingHelper.simpleTypeSpec(TypeIndex.u32)
);

let builder: string[];
let docBuilder: DocBuilder;

beforeEach(() => {
  builder = [];
  const stringifier = new TypeSpecStringifier([]);
  docBuilder = new DocBuilder("- ", builder, stringifier);
});

test("line", () => {
  docBuilder.appendLine("");

  assertContents("- \n");
});

test("section", () => {
  docBuilder.appendParagraph(["A"]);
  assertContents("- A\n- \n");
});

test("emptyParamList", () => {
  docBuilder.appendGenericParamList("Header", [], new Map());
  assertContents("");
});

test("emptyParamDesc", () => {
  docBuilder.appendGenericParamList("Header", [arg1], new Map());
  assertContents("");
});

test("mismatchParamDesc", () => {
  docBuilder.appendGenericParamList("Header", [arg1], new Map([["invalid_arg", "ignored"]]));
  assertContents("");
});

test("nullEmptyParamDesc", () => {
  const descriptions = new HashMap<string, string>();
  descriptions.set("arg2", "not ignored");

  docBuilder.appendGenericParamList("Header", [arg1, arg2], descriptions);

  assertContents("- Header\n" + "- \n" + "- * `arg2`: [`u32`], not ignored\n" + "- \n");
});

test("nonEmptyParamDesc", () => {
  docBuilder.appendGenericParamList("Header", [arg1], new Map([["arg1", "the first argument"]]));

  assertContents("- Header\n" + "- \n" + "- * `arg1`: [`u64`], the first argument\n" + "- \n");
});

test("returns", () => {
  docBuilder.appendReturns("a line of text");

  assertContents("- # Returns:\n" + "- \n" + "- a line of text\n" + "- \n");
});

const assertContents = (expected: string) => {
  expect(docBuilder.build()).toEqual(expected);
};
