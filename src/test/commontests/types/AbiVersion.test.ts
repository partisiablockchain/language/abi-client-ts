/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("valueToString", () => {
  const version = new AbiVersion(1, 2, 3);
  expect(version.toString()).toEqual("1.2.3");
});

test("parseFromString", () => {
  const expected = new AbiVersion(1, 2, 3);
  expect(AbiVersion.parseFromString("1.2.3")).toEqual(expected);
});

test("parseLargeVersionsFromString", () => {
  const expected = new AbiVersion(141, 212, 35);
  expect(AbiVersion.parseFromString("141.212.35")).toEqual(expected);
});

test("failParseMajor", () => {
  const invalidVersion = "a.2.3";
  expect(() => AbiVersion.parseFromString(invalidVersion)).toThrowError(
    "Invalid version: " + invalidVersion
  );
});

test("failParseMajorWithPrefix", () => {
  const invalidVersion = "alpha-2.2.3";
  expect(() => AbiVersion.parseFromString(invalidVersion)).toThrowError(
    "Invalid version: " + invalidVersion
  );
});

test("failParseMinor", () => {
  const invalidVersion = "1.u.3";
  expect(() => AbiVersion.parseFromString(invalidVersion)).toThrowError(
    "Invalid version: " + invalidVersion
  );
});

test("failParseMinorWithInfix", () => {
  const invalidVersion = "1.1u.3";
  expect(() => AbiVersion.parseFromString(invalidVersion)).toThrowError(
    "Invalid version: " + invalidVersion
  );
});

test("failParsePatch", () => {
  const invalidVersion = "1.2.x";
  expect(() => AbiVersion.parseFromString(invalidVersion)).toThrowError(
    "Invalid version: " + invalidVersion
  );
});

test("failParsePatchWithSuffix", () => {
  const invalidVersion = "1.2.3-beta";
  expect(() => AbiVersion.parseFromString(invalidVersion)).toThrowError(
    "Invalid version: " + invalidVersion
  );
});
