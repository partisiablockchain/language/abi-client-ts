/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import {
  AbiVersion,
  ContractAbi,
  FileAbi,
  FnAbi,
  FnKinds,
  RpcReader,
  TypeIndex,
  TypeSpec,
} from "../../main";

export const RpcReaderHelper = {
  wrap(rpcHex: string, arg: FileAbi | TypeSpec): RpcReader {
    let fileAbi;
    if ("typeIndex" in arg) {
      const contract = new ContractAbi(
        [],
        [
          new FnAbi(FnKinds.action, "test", Buffer.from("dddddd0d", "hex"), [
            { name: "value", type: arg },
          ]),
        ],
        { typeIndex: TypeIndex.Named, index: 0 }
      );
      const version: AbiVersion = new AbiVersion(4, 0, 0);
      fileAbi = new FileAbi("PBCABI", version, version, null, contract);
    } else {
      fileAbi = arg;
    }
    return new RpcReader(
      new BigEndianByteInput(Buffer.from(rpcHex, "hex")),
      fileAbi,
      FnKinds.action
    );
  },
};
