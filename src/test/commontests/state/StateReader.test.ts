/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

const u8: SimpleTypeSpec = TestingHelper.simpleTypeSpec(TypeIndex.u8);
const vecU8: VecTypeSpec = TestingHelper.vecTypeSpec(u8);
const setU8: SetTypeSpec = TestingHelper.setTypeSpec(u8);
const mapU8U8: MapTypeSpec = TestingHelper.mapTypeSpec(u8, u8);
const optionU8: OptionTypeSpec = TestingHelper.optionTypeSpec(u8);
const arrayOf4: SizedByteArrayTypeSpec = TestingHelper.sizedByteArrayTypeSpec(4);

const structType: StructTypeSpec = new StructTypeSpec(
  "struct0",
  list(TestingHelper.fieldAbi("field0", u8), TestingHelper.fieldAbi("field1", u8))
);
const structRef: NamedTypeRef = TestingHelper.namedTypeRef(0);

test("readVec", () => {
  const vec: ScValueVector = StateReaderHelper.wrap("0100000010").readStateValue(vecU8).vecValue();
  expect(vec.size()).toEqual(1); // commontests-ignore-array
  expect(vec.get(0).getType()).toEqual(TypeIndex.u8); // commontests-ignore-array
  expect(() => vec.boolValue()).toThrowError("Cannot read bool for current type");
});

test("readString", () => {
  const value: ScValue = StateReaderHelper.wrap("0100000042").readStateValue(
    TestingHelper.simpleTypeSpec(TypeIndex.String)
  );
  expect(value.stringValue()).toEqual("B");
});

test("readSet", () => {
  const set: ScValueSet = StateReaderHelper.wrap("0100000010").readStateValue(setU8).setValue();
  expect(set.size()).toEqual(1); // commontests-ignore-array
  expect(set.get(0).getType()).toEqual(TypeIndex.u8); // commontests-ignore-array
});

test("readMap", () => {
  const map: ScValueMap = StateReaderHelper.wrap("010000001020").readStateValue(mapU8U8).mapValue();
  expect(map.size()).toEqual(1); // commontests-ignore-array
  expect(map.isEmpty()).toBeFalsy();

  const key = new ScValueNumber(TypeIndex.u8, 0x10);
  expect(map.get(key)).toEqual(new ScValueNumber(TypeIndex.u8, 0x20)); // commontests-ignore-array
});

test("readOption", () => {
  const none: ScValueOption = StateReaderHelper.wrap("00").readStateValue(optionU8).optionValue();
  expect(none.isSome()).toBeFalsy();

  const some: ScValueOption = StateReaderHelper.wrap("0101").readStateValue(optionU8).optionValue();
  expect(some.isSome()).toBeTruthy();
  expect(some.innerValue).toEqual(new ScValueNumber(TypeIndex.u8, 1));
});

test("readSizedArray", () => {
  const value: ScValue = StateReaderHelper.wrap("01020304").readStateValue(arrayOf4);
  expect(value.sizedByteArrayValue()).toHaveLength(4);
  const expected: ScValueSizedByteArray = new ScValueSizedByteArray(
    Buffer.from([0x01, 0x02, 0x03, 0x04])
  );
  expect(value).toEqual(expected);
});

test("readStruct", () => {
  const typeRef: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const contract: ContractAbi = new ContractAbi([structType], [], typeRef);
  const state: ScValueStruct = StateReaderHelper.wrap("ff92", contract)
    .readStateValue(structRef)
    .structValue();
  expect(state.size()).toEqual(2); // commontests-ignore-array
  expect(state.getFieldValue("field0")).toEqual(new ScValueNumber(TypeIndex.u8, 0xff));
  expect(state.getFieldValue("field1")).toEqual(new ScValueNumber(TypeIndex.u8, 0x92));

  const valueTest: ScValueStruct = StateReaderHelper.wrap("ff92", contract)
    .readStateValue(structRef)
    .structValue();
  expect(valueTest.size()).toEqual(2); // commontests-ignore-array
  expect(valueTest.getFieldValue("field0")).toEqual(new ScValueNumber(TypeIndex.u8, 0xff));
  expect(valueTest.getFieldValue("field1")).toEqual(new ScValueNumber(TypeIndex.u8, 0x92));
});

test("readSimpleNumber", () => {
  const value = StateReaderHelper.wrap("ff").readSimpleType(TypeIndex.u8);
  expect(value.getType()).toEqual(TypeIndex.u8);
  expect(value.asNumber()).toEqual(255);
});

test("readBNNumber", () => {
  const value1 = StateReaderHelper.wrap("ffffffffffffffffffffffffffffffff").readSimpleType(
    TypeIndex.u128
  );
  expect(value1.getType()).toEqual(TypeIndex.u128);
  expect(value1.asBN().cmp(new BN("340282366920938463463374607431768211455"))).toEqual(0);

  const value2 = StateReaderHelper.wrap(
    "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
  ).readSimpleType(TypeIndex.u256);
  expect(value2.getType()).toEqual(TypeIndex.u256);
  expect(
    value2
      .asBN()
      .cmp(
        new BN(
          "11579208923731619542357098500868" + "7907853269984665640564039457584007913129639935"
        )
      )
  ).toEqual(0);
});

test("readSimpleBool", () => {
  const value1 = StateReaderHelper.wrap("01").readSimpleType(TypeIndex.bool).boolValue();
  expect(value1).toEqual(true);
  const value2 = StateReaderHelper.wrap("ff").readSimpleType(TypeIndex.bool).boolValue();
  expect(value2).toEqual(true);
  const value3 = StateReaderHelper.wrap("00").readSimpleType(TypeIndex.bool).boolValue();
  expect(value3).toEqual(false);
});

test("readSimpleString", () => {
  const expected = "hello";

  const value = StateReaderHelper.wrap("0500000068656c6c6f").readSimpleType(TypeIndex.String);
  expect(value.getType()).toEqual(TypeIndex.String);
  expect(value.stringValue()).toEqual(expected);
});

test("readSimpleAddress", () => {
  const addressBytes = "020101010101010101010101010101010101010101";
  const value = StateReaderHelper.wrap(addressBytes).readSimpleType(TypeIndex.Address);
  expect(value.getType()).toEqual(TypeIndex.Address);
  expect(value.addressValue().value).toEqual(bytesFromHex(addressBytes));
});

test("readHash", () => {
  const hashBytesAsHexString = ValidTestHexValues.HASH;
  const value = StateReaderHelper.wrap(hashBytesAsHexString).readSimpleType(TypeIndex.Hash);
  expect(value.getType()).toEqual(TypeIndex.Hash);
  expect(value.hashValue().value).toEqual(bytesFromHex(hashBytesAsHexString));
});

test("readPublicKey", () => {
  const publicKeyBytesAsHexString = ValidTestHexValues.PUBLIC_KEY;
  const value = StateReaderHelper.wrap(publicKeyBytesAsHexString).readSimpleType(
    TypeIndex.PublicKey
  );
  expect(value.getType()).toEqual(TypeIndex.PublicKey);
  expect(value.publicKeyValue().value).toEqual(bytesFromHex(publicKeyBytesAsHexString));
});

test("readSignature", () => {
  const signatureBytesAsHexString = ValidTestHexValues.SIGNATURE;
  const value = StateReaderHelper.wrap(signatureBytesAsHexString).readSimpleType(
    TypeIndex.Signature
  );
  expect(value.getType()).toEqual(TypeIndex.Signature);
  expect(value.signatureValue().value).toEqual(bytesFromHex(signatureBytesAsHexString));
});

test("readBlsPublicKey", () => {
  const blsPublicKeyBytesAsHexString = ValidTestHexValues.BLS_PUBLIC_KEY;
  const value = StateReaderHelper.wrap(blsPublicKeyBytesAsHexString).readSimpleType(
    TypeIndex.BlsPublicKey
  );
  expect(value.getType()).toEqual(TypeIndex.BlsPublicKey);
  expect(value.blsPublicKeyValue().value).toEqual(bytesFromHex(blsPublicKeyBytesAsHexString));
});

test("readBlsSignature", () => {
  const blsSignatureBytesAsHexString = ValidTestHexValues.BLS_SIGNATURE;
  const value = StateReaderHelper.wrap(blsSignatureBytesAsHexString).readSimpleType(
    TypeIndex.BlsSignature
  );
  expect(value.getType()).toEqual(TypeIndex.BlsSignature);
  expect(value.blsSignatureValue().value).toEqual(bytesFromHex(blsSignatureBytesAsHexString));
});

test("readAvlTreeMap", () => {
  const type = TestingHelper.avlTreeMapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.i32),
    TestingHelper.simpleTypeSpec(TypeIndex.u8)
  );

  const stateBuilder = new StateBuilder(null);
  const mapBuilder = stateBuilder.addMap();
  mapBuilder.addOption().addI32(0);
  const innerMapBuilder = mapBuilder.addOption().addOption().addMap();
  innerMapBuilder.addOption().addOption().addVecU8(bytesFromHex("01000000"));
  innerMapBuilder.addOption().addVecU8(bytesFromHex("01"));
  innerMapBuilder.addOption().addOption().addVecU8(bytesFromHex("02000000"));
  innerMapBuilder.addOption().addVecU8(bytesFromHex("42"));

  const contract = new ContractAbi([], [], TestingHelper.namedTypeRef(0));
  const stateReader = new StateReader(bytesFromHex("00000000"), contract, stateBuilder.getBytes());
  const value = stateReader.readGeneric(type).avlTreeMapValue();
  const key1 = new ScValueNumber(TypeIndex.i32, 1);
  const key2 = new ScValueNumber(TypeIndex.i32, 2);
  expect(value.treeId).toEqual(0);
  const map = requireNonNull(value.map);
  expect(requireNonNull(map.get(key1)).asNumber()).toEqual(1); // commontests-ignore-array
  expect(requireNonNull(map.get(key2)).asNumber()).toEqual(0x42); // commontests-ignore-array
});

test("readAvlTreeMapNoMap", () => {
  const type = TestingHelper.avlTreeMapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.i32),
    TestingHelper.simpleTypeSpec(TypeIndex.u8)
  );
  const contract = new ContractAbi([], [], TestingHelper.namedTypeRef(0));
  const stateReader = new StateReader(bytesFromHex("00000000"), contract);
  const value = stateReader.readGeneric(type).avlTreeMapValue();
  expect(value.treeId).toEqual(0);
  expect(value.map).toBeNull(); // commontests-expect-null
});

test("readAvlTreeMapError", () => {
  const type = TestingHelper.avlTreeMapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.i32),
    TestingHelper.simpleTypeSpec(TypeIndex.u8)
  );
  const contract = new ContractAbi([], [], TestingHelper.namedTypeRef(0));

  const stateReader = new StateReader(bytesFromHex("00000000"), contract, bytesFromHex("00000000"));
  expect(() => stateReader.readGeneric(type)).toThrowError(
    "Tried to read Avl Tree Map with tree id 0, but it didn't exist"
  );
});

test("readNestedAvlTree", () => {
  const type = TestingHelper.avlTreeMapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.i32),
    TestingHelper.avlTreeMapTypeSpec(
      TestingHelper.simpleTypeSpec(TypeIndex.u8),
      TestingHelper.simpleTypeSpec(TypeIndex.bool)
    )
  );

  const stateBuilder = new StateBuilder(null);
  const mapBuilder = stateBuilder.addMap();
  mapBuilder.addOption().addI32(0);
  const innerMapBuilder = mapBuilder.addOption().addOption().addMap();
  innerMapBuilder.addOption().addOption().addVecU8(bytesFromHex("42000000"));
  innerMapBuilder.addOption().addVecU8(bytesFromHex("01000000"));
  mapBuilder.addOption().addI32(1);
  const innerMapBuilder2 = mapBuilder.addOption().addOption().addMap();
  innerMapBuilder2.addOption().addOption().addVecU8(bytesFromHex("00"));
  innerMapBuilder2.addOption().addVecU8(bytesFromHex("01"));
  innerMapBuilder2.addOption().addOption().addVecU8(bytesFromHex("01"));
  innerMapBuilder2.addOption().addVecU8(bytesFromHex("00"));

  const contract = new ContractAbi([], [], TestingHelper.namedTypeRef(0));
  const stateReader = new StateReader(bytesFromHex("00000000"), contract, stateBuilder.getBytes());
  const value = stateReader.readGeneric(type).avlTreeMapValue();
  expect(value.treeId).toEqual(0);

  const map = requireNonNull(
    requireNonNull(value.map).get(new ScValueNumber(TypeIndex.i32, 0x42))
  ).avlTreeMapValue();
  expect(map.treeId).toEqual(1);
  const innerMap = requireNonNull(map.map);
  expect(requireNonNull(innerMap.get(new ScValueNumber(TypeIndex.u8, 0))).boolValue()).toBeTruthy();
  expect(requireNonNull(innerMap.get(new ScValueNumber(TypeIndex.u8, 1))).boolValue()).toBeFalsy();
});

test("stateBytes", () => {
  const stateBytes = TestingHelper.stateBytes(Buffer.alloc(2));
  expect(stateBytes.state).toEqual(Buffer.alloc(2));
  expect(stateBytes.avlTrees).toBeUndefined();
});
