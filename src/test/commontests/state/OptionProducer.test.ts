/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("contractOption", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-options
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_options_v3.abi");
  const stateProducer = new StateBuilder(contractAbi);
  stateProducer.addOption().addU64(1);
  stateProducer.addOption().addString("option");
  stateProducer.addOption().addAddress(bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"));
  stateProducer.addOption().addBool(false);
  stateProducer.addOption();
  const state = stateProducer.getBytes();
  const expected = concatBytes(
    Buffer.from([0x01]),
    bytesFromHex("0100000000000000"),
    Buffer.from([0x01]),
    bytesFromStringLe("option"),
    Buffer.from([0x01]),
    bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"),
    Buffer.from([0x01]),
    Buffer.from([0x00]),
    Buffer.from([0x00])
  );
  expect(state).toEqual(expected);

  // Test missing element
  const producerNoOption = new StateBuilder(contractAbi);
  producerNoOption.addOption();
  expect(() => producerNoOption.getBytes()).toThrowError(
    "In root, Missing argument 'option_string'"
  );

  // Test without type checking
  const producerNoTypeCheck = new StateBuilder(null);
  producerNoTypeCheck.addOption().addString("hello");
  const stateNoTypeCheck = BuilderHelper.builderToBytesLe(producerNoTypeCheck);
  const expectedNoTypeCheck = concatBytes(Buffer.from([0x01]), bytesFromStringLe("hello"));
  expect(stateNoTypeCheck).toEqual(expectedNoTypeCheck);

  // Test without type checking and without option
  const producerNoTypeCheckNoOption = new StateBuilder(null);
  producerNoTypeCheckNoOption.addOption();
  const stateNoTypeCheckNoOption = BuilderHelper.builderToBytesLe(producerNoTypeCheckNoOption);
  expect(stateNoTypeCheckNoOption).toEqual(Buffer.from([0x00]));
});

test("noTypeCheckState", () => {
  const stateProducer = new StateBuilder(null);
  stateProducer.addOption().addU8(0x0a);
  stateProducer.addOption();
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = bytesFromHex("010A00");
  expect(state).toEqual(expected);

  const producerWithTwoOptions = new StateBuilder(null);
  producerWithTwoOptions.addOption().addOption().addI8(-1);
  const stateTwoOptions = BuilderHelper.builderToBytesLe(producerWithTwoOptions);
  const expectedTwoOptions = bytesFromHex("0101ff");
  expect(stateTwoOptions).toEqual(expectedTwoOptions);
});

test("doubleAdd", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(
      TestingHelper.fieldAbi(
        "f",
        TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u32))
      )
    )
  );
  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer = new StateBuilder(simpleContractAbi);
  expect(() => stateProducer.addOption().addU32(1).addU32(1)).toThrowError(
    "In /f, Cannot set option value twice."
  );
});

test("invalidType", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer = new StateBuilder(simpleContractAbi);

  expect(() => stateProducer.addBool(true)).toThrowError("In /f, Expected type u64, but got bool");
});

test("typeCheck", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(
      TestingHelper.fieldAbi(
        "f",
        TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
      )
    )
  );

  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));

  const stateBuilder: StateBuilder = new StateBuilder(simpleContractAbi);

  expect(() => stateBuilder.addOption().addBool(true)).toThrowError(
    "In /f, Expected type u64, but got bool"
  );

  expect(() =>
    BuilderHelper.builderToBytesLe(new StateBuilder(simpleContractAbi).addOption().addOption())
  ).toThrowError("In /f, Expected type u64, but got Option");
});
