/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("assertTypeError", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_simple_map.abi");
  const stateProducer = new StateBuilder(contractAbi);
  expect(() => stateProducer.addBool(true)).toThrowError(
    "In /my_map, Expected type Map, but got bool"
  );

  const mapStateProducer = stateProducer.addMap();
  expect(() => mapStateProducer.addMap().addStruct()).toThrowError(
    "In /my_map/0/key, Expected type Address, but got Map"
  );

  expect(() => mapStateProducer.addAddress(Buffer.alloc(21)).addStruct()).toThrowError(
    "In /my_map/0/value, Expected type u8, but got Named"
  );
});

test("testMap", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_simple_map.abi");
  const stateProducer = new StateBuilder(contractAbi);
  const mapStateProducer = stateProducer.addMap();
  mapStateProducer.addAddress(bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff")).addU8(0);
  mapStateProducer.addAddress(bytesFromHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd")).addU8(1);

  const state = stateProducer.getBytes();
  const expected = concatBytes(
    bytesFromHex("02000000"),
    bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"),
    bytesFromHex("00"),
    bytesFromHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd"),
    bytesFromHex("01")
  );
  expect(state).toEqual(expected);

  // Testing with no type checking
  const producerNoTypeCheck = new StateBuilder(null);
  const mapStateProducerNoTypeCheck = producerNoTypeCheck.addMap();
  mapStateProducerNoTypeCheck
    .addAddress(bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"))
    .addU8(0);
  mapStateProducerNoTypeCheck
    .addAddress(bytesFromHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd"))
    .addU8(1);
  const stateNoTypeCheck = BuilderHelper.builderToBytesLe(mapStateProducerNoTypeCheck);
  expect(stateNoTypeCheck).toEqual(expected);
});

test("missingValueWrite", () => {
  const spec = TestingHelper.mapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.String),
    TestingHelper.simpleTypeSpec(TypeIndex.Address)
  );
  const contract = new ContractAbi(
    list(new StructTypeSpec("struct", list(TestingHelper.fieldAbi("f1", spec)))),
    [],
    TestingHelper.namedTypeRef(0)
  );
  const producer = new StateBuilder(contract);
  producer.addMap().addString("k1");
  expect(() => producer.getBytes()).toThrowError("In /f1, Missing value for key");
});
