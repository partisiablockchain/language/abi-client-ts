/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("toJsonBool", () => {
  const boolValue = new ScValueBool(false);
  ValueHelper.assertValueJson(boolValue, "false");
});

test("toJsonNumbers", () => {
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.u8, 255), "255");
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.i8, -1), "-1");
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.u16, 65535), "65535");
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.i16, -1), "-1");
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.u32, 12345678), "12345678");
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.i32, -1), "-1");
  ValueHelper.assertValueJson(
    new ScValueNumber(TypeIndex.u64, new BN("12345678987654321")),
    '"12345678987654321"'
  );
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.i64, new BN("-1")), '"-1"');
  ValueHelper.assertValueJson(
    new ScValueNumber(TypeIndex.u128, new BN("340282366920938463463374607431768211455")),
    '"340282366920938463463374607431768211455"'
  );
  ValueHelper.assertValueJson(
    new ScValueNumber(
      TypeIndex.u256,
      new BN("115792089237316195423570985008687907853269984665640564039457584007913129639935")
    ),
    '"115792089237316195423570985008687907853269984665640564039457584007913129639935"'
  );
  ValueHelper.assertValueJson(new ScValueNumber(TypeIndex.i128, new BN("-1")), '"-1"');
});

test("toJsonAddress", () => {
  const addressValue = new ScValueAddress(
    bytesFromHex("010000000000000000000000000000000000000020")
  );
  ValueHelper.assertValueJson(addressValue, '"010000000000000000000000000000000000000020"');
});

test("toJsonHash", () => {
  const hashValue = new ScValueHash(bytesFromHex(ValidTestHexValues.HASH));
  ValueHelper.assertValueJson(hashValue, toJsonNodeFormat(ValidTestHexValues.HASH));
});

test("toJsonPublicKey", () => {
  const publicKeyValue = new ScValuePublicKey(bytesFromHex(ValidTestHexValues.PUBLIC_KEY));
  ValueHelper.assertValueJson(publicKeyValue, toJsonNodeFormat(ValidTestHexValues.PUBLIC_KEY));
});

test("toJsonSignature", () => {
  const signatureValue = new ScValueSignature(bytesFromHex(ValidTestHexValues.SIGNATURE));
  ValueHelper.assertValueJson(signatureValue, toJsonNodeFormat(ValidTestHexValues.SIGNATURE));
});

test("toJsonBlsPublicKey", () => {
  const blsPublicKeyValue = new ScValueBlsPublicKey(
    bytesFromHex(ValidTestHexValues.BLS_PUBLIC_KEY)
  );
  ValueHelper.assertValueJson(
    blsPublicKeyValue,
    toJsonNodeFormat(ValidTestHexValues.BLS_PUBLIC_KEY)
  );
});

test("toJsonBlsSignature", () => {
  const blsSignatureValue = new ScValueBlsSignature(bytesFromHex(ValidTestHexValues.BLS_SIGNATURE));
  ValueHelper.assertValueJson(
    blsSignatureValue,
    toJsonNodeFormat(ValidTestHexValues.BLS_SIGNATURE)
  );
});

test("toJsonString", () => {
  const stringValue = new ScValueString("hello");
  ValueHelper.assertValueJson(stringValue, '"hello"');
});

test("toJsonSizedByteArray", () => {
  const arrayValue = new ScValueSizedByteArray(bytesFromHex("aa00f0f0a245623657"));
  ValueHelper.assertValueJson(arrayValue, '"aa00f0f0a245623657"');
});

test("toJsonVec", () => {
  const scValues: ScValue[] = list(
    new ScValueNumber(TypeIndex.i32, 1),
    new ScValueNumber(TypeIndex.i32, 2),
    new ScValueNumber(TypeIndex.i32, 3)
  );
  const vecValue = new ScValueVector(scValues);
  ValueHelper.assertValueJson(vecValue, "[ 1, 2, 3 ]");
});

test("toJsonStruct", () => {
  const map = new Map<string, ScValue>();
  map.set("a", new ScValueBool(true));
  map.set("b", new ScValueBool(false));
  const val = new ScValueStruct("struct", map);
  ValueHelper.assertValueJson(val, "{\n" + '  "a" : true,\n' + '  "b" : false\n' + "}");
});

test("toJsonEnum", () => {
  const fieldsForStruct = new Map<string, ScValue>();
  fieldsForStruct.set("a", new ScValueBool(true));
  fieldsForStruct.set("b", new ScValueBool(false));
  const struct = new ScValueStruct("struct", fieldsForStruct);
  const val = new ScValueEnum("enum", struct);
  ValueHelper.assertValueJson(
    val,
    "{\n" + '  "@type" : "struct",\n' + '  "a" : true,\n' + '  "b" : false\n' + "}"
  );
});

test("toJsonComplexEnum", () => {
  const fieldsForCar = new Map<string, ScValue>();
  fieldsForCar.set("EngineSize", new ScValueNumber(TypeIndex.u32, 10));
  fieldsForCar.set("Length", new ScValueNumber(TypeIndex.u32, 20));
  const carStruct = new ScValueStruct("Car", fieldsForCar);
  const fieldsForBicycle = new Map<string, ScValue>();
  fieldsForBicycle.set("WheelDiameter", new ScValueNumber(TypeIndex.u32, 4));
  const bicycleStruct = new ScValueStruct("Bicycle", fieldsForBicycle);
  const fieldsForStruct = new Map<string, ScValue>();
  fieldsForStruct.set("a", new ScValueEnum("Vehicle", carStruct));
  fieldsForStruct.set("b", new ScValueEnum("Vehicle", bicycleStruct));
  const struct = new ScValueStruct("OneVariant", fieldsForStruct);
  ValueHelper.assertValueJson(
    struct,
    "{\n" +
      '  "a" : {\n' +
      '    "@type" : "Car",\n' +
      '    "EngineSize" : 10,\n' +
      '    "Length" : 20\n' +
      "  },\n" +
      '  "b" : {\n' +
      '    "@type" : "Bicycle",\n' +
      '    "WheelDiameter" : 4\n' +
      "  }\n" +
      "}"
  );
});

test("toJsonSet", () => {
  const values: ScValue[] = list(
    new ScValueNumber(TypeIndex.i32, 1),
    new ScValueNumber(TypeIndex.i32, 2),
    new ScValueNumber(TypeIndex.i32, 3)
  );
  const setValue = new ScValueSet(values);
  ValueHelper.assertValueJson(setValue, "[ 1, 2, 3 ]");
});

test("toJsonOption", () => {
  const optionSome = new ScValueOption(new ScValueNumber(TypeIndex.i32, 42));
  ValueHelper.assertValueJson(
    optionSome,
    "{\n" + '  "isSome" : true,\n' + '  "innerValue" : 42\n' + "}"
  );

  const optionNone = new ScValueOption(null);
  ValueHelper.assertValueJson(optionNone, "{\n" + '  "isSome" : false\n' + "}");
});

test("toJsonMap", () => {
  const map = new HashMap<ScValue, ScValue>();
  map.set(new ScValueNumber(TypeIndex.i32, 42), new ScValueBool(false));
  map.set(new ScValueNumber(TypeIndex.i32, 21), new ScValueBool(true));
  map.set(new ScValueNumber(TypeIndex.i32, 84), new ScValueBool(true));
  const mapValue = new ScValueMap(map);
  ValueHelper.assertValueJson(
    mapValue,
    "[ {\n" +
      '  "key" : 42,\n' +
      '  "value" : false\n' +
      "}, {\n" +
      '  "key" : 21,\n' +
      '  "value" : true\n' +
      "}, {\n" +
      '  "key" : 84,\n' +
      '  "value" : true\n' +
      "} ]"
  );
});

test("testTokenContract", () => {
  ValueHelper.assertValueJson(
    ValueHelper.loadState("token_contract.abi", "token_contract.state"),
    "{\n" +
      '  "name" : "Token Name",\n' +
      '  "decimals" : 18,\n' +
      '  "symbol" : "SYMB",\n' +
      '  "owner" : "020000000000000000000000000000000000000001",\n' +
      '  "total_supply" : "123123123",\n' +
      '  "balances" : [ {\n' +
      '    "key" : "020000000000000000000000000000000000000001",\n' +
      '    "value" : "123123123"\n' +
      "  } ],\n" +
      '  "allowed" : [ ]\n' +
      "}"
  );
});

test("initialAuctionState", () => {
  ValueHelper.assertValueJson(
    ValueHelper.loadState("rust_example_auction_contract.abi", "auction_initial.state"),
    "{\n" +
      '  "contract_owner" : "010000000000000000000000000000000000000001",\n' +
      '  "start_time" : "12346",\n' +
      '  "end_time" : "12423",\n' +
      '  "token_amount_for_sale" : "100",\n' +
      '  "commodity_token_type" : "02eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",\n' +
      '  "currency_token_type" : "02dddddddddddddddddddddddddddddddddddddddd",\n' +
      '  "highest_bidder" : {\n' +
      '    "bidder" : "010000000000000000000000000000000000000001",\n' +
      '    "amount" : "0"\n' +
      "  },\n" +
      '  "reserve_price" : "20",\n' +
      '  "min_increment" : "5",\n' +
      '  "claim_map" : [ ],\n' +
      '  "status" : 0\n' +
      "}"
  );
});

test("stateTest", () => {
  const stateStruct = new StructTypeSpec(
    "State",
    list(
      TestingHelper.fieldAbi("active", TestingHelper.simpleTypeSpec(TypeIndex.bool)),
      TestingHelper.fieldAbi("id", TestingHelper.simpleTypeSpec(TypeIndex.i32)),
      TestingHelper.fieldAbi(
        "list",
        TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
      ),
      TestingHelper.fieldAbi("point", TestingHelper.namedTypeRef(1))
    )
  );
  const pointStruct = new StructTypeSpec(
    "Point",
    list(
      TestingHelper.fieldAbi("left", TestingHelper.simpleTypeSpec(TypeIndex.i32)),
      TestingHelper.fieldAbi("right", TestingHelper.simpleTypeSpec(TypeIndex.i32))
    )
  );

  const contract = new ContractAbi([stateStruct, pointStruct], [], TestingHelper.namedTypeRef(0));
  const stateHex =
    "01" + // boolean: true
    "02000000" + // i32: 2
    "02000000" + // Vec<u64> length: 2
    "0300000000000000" + // Vec[0]: 3
    "0400000000000000" + // Vec[1]: 4
    "05000000" + // Point.left: 5
    "06000000"; // Point.right: 6
  const stateVal = new StateReader(
    new LittleEndianByteInput(bytesFromHex(stateHex)),
    contract
  ).readState();

  ValueHelper.assertValueJson(
    stateVal,
    "{\n" +
      '  "active" : true,\n' +
      '  "id" : 2,\n' +
      '  "list" : [ "3", "4" ],\n' +
      '  "point" : {\n' +
      '    "left" : 5,\n' +
      '    "right" : 6\n' +
      "  }\n" +
      "}"
  );
});

test("vecU8small", () => {
  const vec = new ScValueVector(
    list(
      new ScValueNumber(TypeIndex.u8, 1),
      new ScValueNumber(TypeIndex.u8, 2),
      new ScValueNumber(TypeIndex.u8, 3)
    )
  );
  ValueHelper.assertValueJson(vec, '"010203"');
});

test("vecU8empty", () => {
  const vec = new ScValueVector([]);
  ValueHelper.assertValueJson(vec, "[]");
});

test("vecU8length256", () => {
  const values: ScValue[] = [];
  for (let i = 0; i < 256; i++) {
    values.push(new ScValueNumber(TypeIndex.u8, 0));
  }
  const vec = new ScValueVector(values);
  ValueHelper.assertValueJson(
    vec,
    '"00000000000000000000000000000000' +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      '"'
  );
});

test("vecU8Long", () => {
  const values: ScValue[] = [];
  for (let i = 0; i < 280; i++) {
    values.push(new ScValueNumber(TypeIndex.u8, 0));
  }
  const vec = new ScValueVector(values);
  ValueHelper.assertValueJson(
    vec,
    '"00000000000000000000000000000000' +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      "00000000000000000000000000000000" +
      '..."'
  );
});

test("toJsonAvlTreeMap", () => {
  const simpleVal = new ScValueAvlTreeMap(0, null);
  ValueHelper.assertValueJson(simpleVal, "{\n" + '  "treeId": 0\n' + "}");

  const map = new HashMap<ScValue, ScValue>();
  map.set(new ScValueNumber(TypeIndex.i32, 42), new ScValueBool(false));
  map.set(new ScValueNumber(TypeIndex.i32, 21), new ScValueBool(true));
  map.set(new ScValueNumber(TypeIndex.i32, 84), new ScValueBool(true));
  const avlTreeMapValue = new ScValueAvlTreeMap(0, map);
  ValueHelper.assertValueJson(
    avlTreeMapValue,
    "{\n" +
      '  "treeId": 0,\n' +
      '  "map": [\n' +
      "    {\n" +
      '      "key" : 42,\n' +
      '      "value" : false\n' +
      "    }, {\n" +
      '      "key" : 21,\n' +
      '      "value" : true\n' +
      "    }, {\n" +
      '      "key" : 84,\n' +
      '      "value" : true\n' +
      "    }\n" +
      "  ]\n" +
      "}"
  );
});

const toJsonNodeFormat = (string: string): string => {
  return JSON.stringify(string);
};
