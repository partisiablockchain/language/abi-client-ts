/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("parseZkOnSecretInputOnChain", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("onChainOutputRpc.txt");
  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("on_chain_output");
  expect(rpcFn.arguments[0].asNumber()).toEqual(5);
  expect(rpcFn.arguments[1].vecU8Value().length).toEqual(32);
});

test("unableToCalculate", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("unableToCalculateRpc.txt");

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("unable_to_calculate");
  expect(rpcFn.arguments[0].asBN().eq(new BN("1"))).toBeTruthy();
});

test("openMaskedInput", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("openMaskedInputRpc.txt");

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("open_masked_input");
  expect(rpcFn.arguments[0].asNumber()).toEqual(3);
  expect(rpcFn.arguments[1].vecU8Value().length).toEqual(32);
});

test("zkInputOffChain", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("zkInputOffChainRpc.txt");

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_vote");
});

test("zkInputOnChain", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_voting_simple.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("zkInputOnChainRpc.txt");

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_vote");
  expect(rpcFn.arguments.length).toEqual(0);
});

test("zkInputOnChainBid", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "addBidOnChainInput-" + "13bff76ba288b18b269c5a52104e063880c21f313aca8f0ddb865f98a670631f.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_bid");
  expect(rpcFn.arguments.length).toEqual(0);
});

test("zkInputOnChainBid2", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "addBidOnChainInput-" + "13bff76ba288b18b269c5a52104e063880c21f313aca8f0ddb865f98a670631f.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_bid");
  expect(rpcFn.arguments.length).toEqual(0);
});

test("zkInputOnChainIcrc", () => {
  const abi = TestingHelper.loadAbiParserFromFile("icrc.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onSecretInputOnChain-" + "f6c3b0a19a057c56919c1875256641cd2c3af1589c5669a4c42bc65dc2f6bc79.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("on_secret_input");
  expect(rpcFn.arguments.length).toEqual(2);
});

test("offChainInputBid", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "addBidOffChainInput-" + "a80593b37f98f4e3ed0de39361e4bd1b2f97f9242fc2c7c8b22bd127eb564a59.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_bid");
  expect(rpcFn.arguments.length).toEqual(0);
});

test("commitResultVariable", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "commitResultVariable-" + "09670cb2477596ab2254b5eea21331729e98f8cbd0215b55944efb88658709a3.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("commit_result_variable");
  expect(rpcFn.arguments[0].asBN().eq(new BN("2110674"))).toBeTruthy();
  expect(rpcFn.arguments[1].vecValue().values().length).toEqual(1);
});

test("extendZkComputation", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "extendZkComputation-" + "4c5c658984b097efe2e250bba74d812eab74e10028f72d64ea9306b6f95789dd.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("extend_zk_computation_deadline");
  expect(rpcFn.arguments[0].asBN().eq(new BN(new BN("3024")))).toBeTruthy();
  expect(rpcFn.arguments[1].asBN().eq(new BN(new BN("1")))).toBeTruthy();
  expect(rpcFn.arguments[2].asBN().eq(new BN(new BN("86400000")))).toBeTruthy();
  expect(rpcFn.arguments[3].asBN().eq(new BN(new BN("15724800000")))).toBeTruthy();
});

test("rejectInput", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("rejectInputRpc.txt");

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("reject_input");
  expect(rpcFn.arguments[0].asNumber()).toEqual(66);
});

test("onComputeComplete", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onComputeComplete-" + "3c9349bf60aea72830f5b039709f87db26bdc75fd18f6dbd2f5bbc24bc87317b.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("on_compute_complete");
  expect(rpcFn.arguments[0].vecValue().values().length).toEqual(2);
  expect(rpcFn.arguments[0].vecValue().values()[0].asNumber()).toEqual(40);
  expect(rpcFn.arguments[0].vecValue().values()[1].asNumber()).toEqual(41);
});

test("addAttestationSignature", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile("addAttestationSignatureRpc.txt");

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_attestation_signature");
  expect(rpcFn.arguments[0].asNumber()).toEqual(1);
  expect(rpcFn.arguments[1].signatureValue().value.length).toEqual(65);
});

test("addAttestationSignatureTx", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "addAttestationSignature-" +
      "eb2222f6b49f0eda0b080f0510c24edfa149d2bbe5c2331b2dbd72604eeb3fe2.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_attestation_signature");
  expect(rpcFn.arguments[0].asNumber()).toEqual(1);
  expect(rpcFn.arguments[1].signatureValue().value.length).toEqual(65);
});

test("onAttestationComplete", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onAttestationComplete-" +
      "5ded07939ccaff8f7275b4d46f1838954e92c7d6ddb299976ed1e679e70ab7e2.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("on_attestation_complete");
  expect(rpcFn.arguments[0].asNumber()).toEqual(1);
});

test("onVariableInputted", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onVariableInputted-" + "8e0352e5a99316685c7bb99b67414c2bb9473b26a0ed386a83c299f13926f9a4.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("on_variable_inputted");
  expect(rpcFn.arguments[0].asNumber()).toEqual(2);
});

test("onSecretInputOnChainWithSecret", () => {
  const abi = TestingHelper.loadAbiParserFromFile("icrc.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onSecretInputOnChain-" + "f6c3b0a19a057c56919c1875256641cd2c3af1589c5669a4c42bc65dc2f6bc79.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readSecretInputRpc();

  expect(rpcFn.fnAbi.name).toEqual("on_secret_input");
  expect(rpcFn.arguments.length).toEqual(5);
});

test("onSecretInputOffChainWithSecret", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "addBidOffChainInput-" + "a80593b37f98f4e3ed0de39361e4bd1b2f97f9242fc2c7c8b22bd127eb564a59.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readSecretInputRpc();

  expect(rpcFn.fnAbi.name).toEqual("add_bid");
  expect(rpcFn.arguments[0].vecValue().values()[0].asNumber()).toEqual(32);
  expect(rpcFn.arguments[1].hashValue().value.length).toEqual(32);
  expect(rpcFn.arguments[2].hashValue().value.length).toEqual(32);
  expect(rpcFn.arguments[3].hashValue().value.length).toEqual(32);
  expect(rpcFn.arguments[4].hashValue().value.length).toEqual(32);
});

test("getComputationDeadline", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "getComputationDeadline-" +
      "5e869d0fa21c67aaeeb466dc3adcf39132b4e9760b4c4323974c18c6baf5e252.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, abi, FnKinds.action).readRpc();

  expect(rpcFn.fnAbi.name).toEqual("get_computation_deadline");
});

test("getSecretInputOnWrongInvocation", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "getComputationDeadline-" +
      "5e869d0fa21c67aaeeb466dc3adcf39132b4e9760b4c4323974c18c6baf5e252.txt"
  );

  const rpcReader = new RpcReader(invocationBytes, abi, FnKinds.action);
  expect(() => rpcReader.readSecretInputRpc()).toThrowError(
    "Invocation was not a secret input invocation."
  );
});

test("onVariablesOpened", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onVariablesOpened-" + "9dc5c3835e084f83173ac5c865fb817b511a83352d68ad51783f74f8c487bde8.txt"
  );

  const rpcReader = new RpcReader(invocationBytes, abi, FnKinds.action);
  const rpcFn = rpcReader.readRpc();
  expect(rpcFn.fnAbi.name).toEqual("on_variables_opened");
  expect(rpcFn.arguments[0].vecValue().values()[0].asNumber()).toEqual(4);
});

test("addBatchesTriple", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const rpc = Buffer.from([0x12, 0x01, 0x00, 0x00, 0x00, 0x01]);
  const rpcReader = new RpcReader(rpc, abi, FnKinds.action);
  const rpcFn = rpcReader.readRpc();
  expect(rpcFn.fnAbi.name).toEqual("add_batches");
  expect(rpcFn.arguments[0].enumValue().name).toEqual("BatchType");
  expect(rpcFn.arguments[0].enumValue().item.name).toEqual("triple");
  expect(rpcFn.arguments[1].asNumber()).toEqual(1);
});

test("addBatchesInputMask", () => {
  const abi = TestingHelper.loadAbiParserFromFile("zk_second_price.abi").parseAbi();
  const rpc = Buffer.from([0x12, 0x00, 0x00, 0x00, 0x00, 0x02]);
  const rpcReader = new RpcReader(rpc, abi, FnKinds.action);
  const rpcFn = rpcReader.readRpc();
  expect(rpcFn.fnAbi.name).toEqual("add_batches");
  expect(rpcFn.arguments[0].enumValue().item.name).toEqual("input_mask");
  expect(rpcFn.arguments[1].asNumber()).toEqual(2);
});
