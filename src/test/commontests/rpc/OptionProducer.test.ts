/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("contractOption", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-options
  const builder = TestingHelper.createBuilderFromFile("contract_options_v3.abi", "update_u64");
  builder.addOption();
  const rpc = builder.getBytes();
  const expected = concatBytes(bytesFromHex("cf9cffe90b"), Buffer.from([0x00]));
  expect(rpc).toEqual(expected);
  const builderWithOption = TestingHelper.createBuilderFromFile(
    "contract_options_v3.abi",
    "update_string"
  );
  builderWithOption.addOption().addString("hello");

  const hasOptionRpc = builderWithOption.getBytes();
  const hasOptionExpected = concatBytes(
    bytesFromHex("f3eae9b808"),
    Buffer.from([0x01]),
    bytesFromStringBe("hello")
  );
  expect(hasOptionRpc).toEqual(hasOptionExpected);

  // Test without type checking
  const builderNoTypeCheck = new FnRpcBuilder(bytesFromHex("f3eae9b808"));
  builderNoTypeCheck.addOption().addString("hello");
  const rpcNoTypeCheck = builderWithOption.getBytes();
  expect(rpcNoTypeCheck).toEqual(hasOptionExpected);

  const builderNoTypeCheckNoOption = new FnRpcBuilder(bytesFromHex("cf9cffe90b"));
  builderNoTypeCheckNoOption.addOption();
  const test = builderNoTypeCheckNoOption.getBytes();
  expect(test).toEqual(expected);
});

test("noTypeCheckRpc", () => {
  const builder = new FnRpcBuilder(Buffer.alloc(4));
  builder.addOption().addU8(0x0a);
  builder.addOption();
  const rpc = builder.getBytes();
  const expected = bytesFromHex("00000000010A00");
  expect(rpc).toEqual(expected);

  const builderWithTwoOptions = new FnRpcBuilder(Buffer.alloc(4));
  builderWithTwoOptions.addOption().addOption().addI8(-1);
  const rpcTwoOptions = builderWithTwoOptions.getBytes();
  const expectedTwoOptions = bytesFromHex("000000000101ff");
  expect(rpcTwoOptions).toEqual(expectedTwoOptions);
});

test("doubleAdd", () => {
  const builder = new FnRpcBuilder(Buffer.alloc(4));
  expect(() => builder.addOption().addU8(0x0a).addU32(1)).toThrowError(
    "In , Cannot set option value twice."
  );
});

test("invalidType", () => {
  const initShortname = bytesFromHex("fffffffff0");
  const abi: FnAbi = new FnAbi(
    FnKinds.init,
    "init",
    initShortname,
    list(TestingHelper.argumentAbi("int64", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const simpleContractAbi = new ContractAbi([], [abi], TestingHelper.namedTypeRef(0));
  const builder = new FnRpcBuilder("init", simpleContractAbi);
  expect(() => builder.addBool(true)).toThrowError(
    "In init/int64, Expected type u64, but got bool"
  );
});

test("typeCheck", () => {
  const initShortname = bytesFromHex("fffffffff0");
  const abi: FnAbi = new FnAbi(
    FnKinds.init,
    "init",
    initShortname,
    list(
      TestingHelper.argumentAbi(
        "option",
        TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
      )
    )
  );

  const contract = new ContractAbi([], [abi], TestingHelper.namedTypeRef(0));

  expect(() => new FnRpcBuilder("init", contract).addOption().addBool(true)).toThrowError(
    "In init/option, Expected type u64, but got bool"
  );

  expect(() => new FnRpcBuilder("init", contract).addOption().addOption()).toThrowError(
    "In init/option, Expected type u64, but got Option"
  );
});
