/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("fieldNameIsEmptyWhenTypeSpecIsNull", () => {
  const producer: EnumVariantProducer = new EnumVariantProducer(null, 0, "");
  expect(producer.getFieldName()).toEqual("/");
});

test("assertTypeErrorInEnum", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/testing-types/-/tree/main/contract-enum
  const builder = TestingHelper.createBuilderFromFile("contract_enum.abi", "update_enum");
  expect(() => builder.addBool(false)).toThrowError(
    "In update_enum/val, Expected type Named, but got bool"
  );

  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const noEnumType = TestingHelper.createBuilderFromFile(
    "contract_booleans_v3.abi",
    "update_my_bool"
  );
  expect(() => noEnumType.addEnumVariant(0)).toThrowError(
    "In update_my_bool/value, Expected type bool, but got Named"
  );

  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_enum.abi");
  const stateProducer = new StateBuilder(contractAbi);

  expect(() => stateProducer.addEnumVariant(2).addU64(10)).toThrowError(
    "In /my_enum/Bicycle/wheel_diameter, Expected type i32, but got u64"
  );
});

test("missingArgument", () => {
  const enumVariant: StructTypeSpec = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const enumVariantRef: NamedTypeRef = TestingHelper.namedTypeRef(0);

  const enumTypeSpec: EnumTypeSpec = new EnumTypeSpec(
    "enum",
    list(TestingHelper.enumVariant(2, enumVariantRef))
  );

  const state: StructTypeSpec = new StructTypeSpec(
    "State",
    list(TestingHelper.fieldAbi("my_enum", TestingHelper.namedTypeRef(1)))
  );

  const stateRef: NamedTypeRef = TestingHelper.namedTypeRef(2);

  const contractAbi: ContractAbi = new ContractAbi(
    [enumVariant, enumTypeSpec, state],
    [],
    stateRef
  );
  const producer = new StateBuilder(contractAbi);
  producer.addEnumVariant(2);

  expect(() => producer.getBytes()).toThrowError("In /my_enum, Missing argument 'f1'");
});
