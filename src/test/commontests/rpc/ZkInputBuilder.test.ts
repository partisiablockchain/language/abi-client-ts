/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/partisiablockchain/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import { DocBuilder } from "../../../main/parser/RustSyntaxPrettyPrinter";
import { TypeSpecStringifier } from "../../../main/parser/TypeSpecStringifier";
import { ExecutedTransaction } from "../../../main/transaction/ExecutedTransaction";
import { DocumentationFunction } from "../../../main/types/DocumentationFunction";
import { DocumentationNamedType } from "../../../main/types/DocumentationNamedType";
import {
  arraycopy,
  bytesFromBase64,
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueHash,
  ScValuePublicKey,
  ScValueSignature,
  ScValueBlsPublicKey,
  ScValueBlsSignature,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
  RustSyntaxPrettyPrinter,
  TransactionReader,
  BigEndianReader,
  ZkInputBuilder,
  StateBuilder,
  TransactionBuilder,
  AbiOutputBits,
  AbiOutputBytes,
  AvlTreeMapTypeSpec,
  ScValueAvlTreeMap,
  StateBytes,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
  BitOutput,
  BitInput,
} from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { ValidTestHexValues } from "../../ValidTestHexValues";
import { StructProducer } from "../../../main/builder/StructProducer";
import { EnumVariantProducer } from "../../../main/builder/EnumVariantProducer";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("secretInputType", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
  const abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseAbi().contract;
  const zkInputBuilder = ZkInputBuilder.createZkInputBuilder("swap", abi);

  expect(() => zkInputBuilder.addBool(true)).toThrowError(
    "In secret_input, Expected type Named, but got bool"
  );

  const structBuilder = zkInputBuilder.addStruct();
  structBuilder.addI128(new BN("123"));
  structBuilder.addI8(1);
  const bits = zkInputBuilder.getBits();
  expect(bits.data).toEqual(bytesFromHex("7b00000000000000000000000000000001"));
});

test("secretInputOld", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
  const abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseAbi().contract;
  const zkInputBuilder = ZkInputBuilder.createZkInputBuilder("add_vote", abi);

  expect(() => zkInputBuilder.addBool(true)).toThrowError(
    "In secret_input, Expected type i32, but got bool"
  );

  zkInputBuilder.addI32(123);
  const bits = zkInputBuilder.getBits();
  expect(bits.data).toEqual(bytesFromHex("7b000000"));
});

test("nonSecretFunction", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
  const abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseAbi().contract;
  expect(() => ZkInputBuilder.createZkInputBuilder("non_existent", abi)).toThrowError(
    "Contract does not have function with name non_existent"
  );
  expect(() => ZkInputBuilder.createZkInputBuilder("deposit", abi)).toThrowError(
    "Function deposit is not a secret input function"
  );
});

test("addOtherThanOneElement", () => {
  // Contract source can be found at:
  // https://gitlab.com/partisiablockchain/language/contracts/zk-liquidity-swap/-/tree/PAR-7244_secret_input_ABI
  const abi = TestingHelper.loadAbiParserFromFile("zk_liquidity_swap.abi").parseAbi().contract;
  const zkInputBuilder = ZkInputBuilder.createZkInputBuilder("swap", abi);

  expect(() => zkInputBuilder.getBits()).toThrowError("Missing secret input");
  zkInputBuilder.addStruct();
  expect(() => zkInputBuilder.addStruct()).toThrowError("Cannot add secret input twice");
});

test("addBoolean", () => {
  const abi = new ContractAbi(
    list(
      new StructTypeSpec(
        "SecretStruct",
        list(
          TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.bool)),
          TestingHelper.fieldAbi("f2", TestingHelper.simpleTypeSpec(TypeIndex.u8))
        )
      )
    ),
    list(
      new FnAbi(
        FnKinds.zkSecretInputWithExplicitType,
        "secret",
        Buffer.from([1]),
        [],
        TestingHelper.argumentAbi("secret_input", TestingHelper.namedTypeRef(0))
      )
    ),
    TestingHelper.namedTypeRef(0)
  );

  const zkInputBuilder = ZkInputBuilder.createZkInputBuilder("secret", abi);
  zkInputBuilder.addStruct().addBool(true).addU8(42);
  const bits = zkInputBuilder.getBits();
  expect(bits.length).toEqual(9);
  const reader = new BitInput(bits.data);
  expect(reader.readBoolean()).toBeTruthy();
  expect(reader.readUnsignedNumber(8)).toEqual(42);
});
