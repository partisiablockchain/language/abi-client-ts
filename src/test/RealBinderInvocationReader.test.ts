/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { RealBinderInvocationReader } from "../main/rpc/RealBinderInvocationReader";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";
import { AbiParser, FnKinds, RpcReader } from "../main";
import { REAL_BINDER_ABI } from "../main/RealBinderInvocationDeserialization";
import { TestingHelper } from "./commontests/TestingHelper";

test("Get RealBinder invocation", () => {
  const fileAbi = new AbiParser(REAL_BINDER_ABI).parseAbi();
  const realBinderReader = new RealBinderInvocationReader(
    new BigEndianByteInput(Buffer.from([])),
    fileAbi,
    new RpcReader(new BigEndianByteInput(Buffer.from([])), fileAbi, FnKinds.action)
  );
  const noInvocation = realBinderReader.getRealBinderInvocation(
    Buffer.from([0xff]),
    FnKinds.action
  );
  expect(noInvocation.name).toEqual("Unknown Invocation");
  expect(noInvocation.arguments.length).toEqual(0);
});

test("OnSecretInputOnChain invocation.", () => {
  const contractAbi = TestingHelper.loadAbiParserFromFile("icrc.abi").parseAbi();
  const invocationBytes = TestingHelper.readPayloadFile(
    "onSecretInputOnChain-" + "f6c3b0a19a057c56919c1875256641cd2c3af1589c5669a4c42bc65dc2f6bc79.txt"
  );

  const rpcFn = new RpcReader(invocationBytes, contractAbi, FnKinds.action).readSecretInputRpc();
  expect(rpcFn.fnAbi.name).toEqual("on_secret_input");
  expect(rpcFn.arguments[2].vecValue().values().length).toEqual(448);
});
