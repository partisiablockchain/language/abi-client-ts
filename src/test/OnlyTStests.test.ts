/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  AbiParser,
  AbstractBuilder,
  Configuration,
  FileAbi,
  fromKindId,
  FunctionFormat,
  hashBuffer,
  NamedTypesFormat,
  NamedTypeSpec,
  RpcValueFn,
  RustSyntaxPrettyPrinter,
  ScValue,
  ScValueAvlTreeMap,
  ShortnameType,
} from "../main";
import { TestingHelper } from "./commontests/TestingHelper";

test("weird coverage for index", () => {
  expect(hashBuffer.name).toEqual("hashBuffer");
  expect(NamedTypeSpec.name).toEqual("NamedTypeSpec");
  expect(NamedTypesFormat.OnlyStructs).toEqual(1);
  expect(ShortnameType.hash).toEqual(1);
  expect(FunctionFormat.FnKind).toEqual(1);
  expect(Configuration.name).toEqual("Configuration");
  expect(fromKindId.name).toEqual("fromKindId");
  expect(ScValue.name).toEqual("ScValue");
  expect(RpcValueFn.name).toEqual("RpcValueFn");
  expect(RustSyntaxPrettyPrinter.name).toEqual("RustSyntaxPrettyPrinter");
  expect(AbstractBuilder.name).toEqual("AbstractBuilder");
  expect(ScValueAvlTreeMap.name).toEqual("ScValueAvlTreeMap");
});

test("number compare", () => {
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("zk_contract.abi");
  const model: FileAbi = parser.parseAbi();

  const printer = new RustSyntaxPrettyPrinter(model);
  expect(printer.numberCompare(1, 2)).toEqual(-1);
  expect(printer.numberCompare(2, 1)).toEqual(1);
});
