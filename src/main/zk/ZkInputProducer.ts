/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Producer } from "../types/Producer";
import { TypeIndex, TypeSpec } from "../types/Abi";
import { FnAbi } from "../types/FnAbi";
import { AggregateProducer } from "../types/AggregateProducer";
import { AbiOutput } from "../abioutput/AbiOutput";

export class ZkInputProducer implements AggregateProducer {
  private readonly fn;
  private element: Producer | undefined;

  public constructor(fn: FnAbi) {
    this.fn = fn;
  }

  addElement(argument: Producer): void {
    if (this.element !== undefined) {
      throw Error("Cannot add secret input twice");
    } else {
      this.element = argument;
    }
  }

  getFieldName(): string {
    return "secret_input";
  }

  /**
   * Returns the TypeSpec of the secret argument. If kind is ZkSecretInput a default type of i32 is
   * returned.
   *
   * @returns the TypeSpec of the secret argument
   */
  getTypeSpecForElement(): TypeSpec {
    return this.fn.secretArgument?.type ?? { typeIndex: TypeIndex.i32 };
  }

  write(out: AbiOutput): void {
    if (this.element === undefined) {
      throw new Error("Missing secret input");
    } else {
      this.element.write(out);
    }
  }
}
