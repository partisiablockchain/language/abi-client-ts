/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbstractBuilder } from "../builder/AbstractBuilder";
import { ContractAbi } from "../parser/ContractAbi";
import { FnKinds } from "../parser/FnKinds";
import { ZkInputProducer } from "./ZkInputProducer";
import { AbiOutputBits } from "../abioutput/AbiOutputBits";
import { BitOutput, CompactBitArray } from "@secata-public/bitmanipulation-ts";

export class ZkInputBuilder extends AbstractBuilder {
  constructor(contractAbi: ContractAbi, zkInputProducer: ZkInputProducer) {
    super(contractAbi, "", zkInputProducer);
  }

  public static createZkInputBuilder(name: string, contractAbi: ContractAbi) {
    const fn = contractAbi.getFunctionByName(name);
    if (fn === undefined) {
      throw new Error(`Contract does not have function with name ${name}`);
    }
    if (fn.kind !== FnKinds.zkSecretInput && fn.kind !== FnKinds.zkSecretInputWithExplicitType) {
      throw Error(`Function ${name} is not a secret input function`);
    }
    return new ZkInputBuilder(contractAbi, new ZkInputProducer(fn));
  }

  public getBits(): CompactBitArray {
    return BitOutput.serializeBits((out) => {
      this.getAggregateProducer().write(new AbiOutputBits(out));
    });
  }
}
