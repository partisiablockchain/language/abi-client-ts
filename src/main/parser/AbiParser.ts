/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  EnumVariant,
  FieldAbi,
  FnKind,
  FunctionFormat,
  Header,
  NamedTypeIndex,
  NamedTypesFormat,
  NamedTypeSpec,
  ShortnameType,
  TypeIndex,
  TypeSpec,
} from "../types/Abi";
import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { AbiVersion } from "../types/AbiVersion";
import { DocumentationNamedType } from "../types/DocumentationNamedType";
import { EnumTypeSpec } from "../types/EnumTypeSpec";
import { FileAbi } from "../types/FileAbi";
import { FnAbi } from "../types/FnAbi";
import { StructTypeSpec } from "../types/StructTypeSpec";
import { hashBuffer } from "../util/BufferUtil";
import { toHexString } from "../util/NumberToHex";
import { Configuration } from "./Configuration";
import { ContractAbi } from "./ContractAbi";
import { FnKinds, fromKindId } from "./FnKinds";
import { RustSyntaxPrettyPrinter } from "./RustSyntaxPrettyPrinter";

export class AbiParser {
  private readonly bufferReader: BigEndianByteInput;

  constructor(buffer: Buffer) {
    this.bufferReader = new BigEndianByteInput(buffer);
  }

  public parseHeader(): Header {
    const headerBuffer = this.bufferReader.readBytes(6);
    const header = headerBuffer.toString("ascii");

    const versionBinder = new AbiVersion(
      this.bufferReader.readU8(),
      this.bufferReader.readU8(),
      this.bufferReader.readU8()
    );

    const versionClient = new AbiVersion(
      this.bufferReader.readU8(),
      this.bufferReader.readU8(),
      this.bufferReader.readU8()
    );

    return { header, versionBinder, versionClient };
  }

  public parseContract(header: Header): FileAbi {
    const shortnameLength = this.readShortnameLength(header);
    const configuration = Configuration.fromClientVersion(header.versionClient, shortnameLength);
    const contractAbi = this.parseContractAbi(configuration);
    return new FileAbi(
      header.header,
      header.versionBinder,
      header.versionClient,
      shortnameLength,
      contractAbi
    );
  }

  private readShortnameLength(header: Header): number | null {
    if (header.header !== "PBCABI") {
      throw new Error(`Malformed header bytes, expecting PBCABI but was ${header.header}`);
    }

    const options = Configuration.getOptionsOrThrow(header.versionClient);

    let shortnameLength;
    if (options.shortnameType === ShortnameType.hash) {
      shortnameLength = this.bufferReader.readU8();
    } else {
      shortnameLength = null;
    }

    return shortnameLength;
  }

  public parseAbi(): FileAbi {
    const header = this.parseHeader();
    return this.parseContract(header);
  }

  private parseContractAbi(config: Configuration): ContractAbi {
    const namedTypes = this.parseNamedTypes(config);

    let functions;
    if (config.fnType === FunctionFormat.InitSeparately) {
      const init = this.parseFnAbi(config, FnKinds.init);
      functions = this.parseActions(config);
      functions.push(init);
    } else {
      functions = this.parseActions(config);
    }

    // Check for a valid number of functions of each kind.
    for (const [action, kind] of Object.entries(FnKinds)) {
      // Hack to change error message to match Java
      const actionName = action.charAt(0).toUpperCase().concat(action.substring(1));
      const count = functions.filter((a) => a.kind.kindId === kind.kindId).length;
      const validNumberOfFunctions =
        kind.minAllowedPerContract <= count && count <= kind.maxAllowedPerContract;
      if (!validNumberOfFunctions) {
        throw new Error(
          `Expected between ${kind.minAllowedPerContract} and ${kind.maxAllowedPerContract} ${actionName} functions, but found ${count}`
        );
      }
    }

    const stateType = this.parseTypeSpec();

    const remainingSize = this.bufferReader.readRemaining().length;
    if (remainingSize !== 0) {
      throw new Error(
        `Expected EOF after parsed ABI, but stream had ${remainingSize} bytes remaining`
      );
    }

    functions.sort((a, b) => a.kind.kindId - b.kind.kindId);

    return new ContractAbi(namedTypes, functions, stateType);
  }

  private parseNamedTypes(config: Configuration): NamedTypeSpec[] {
    const namedTypesSize = this.bufferReader.readI32();
    const namedTypes: NamedTypeSpec[] = [];
    for (let i = 0; i < namedTypesSize; i++) {
      if (config.namedTypesFormat == NamedTypesFormat.StructsAndEnum) {
        const namedType = this.bufferReader.readU8();
        if (namedType === NamedTypeIndex.Struct) {
          namedTypes.push(this.parseStructTypeSpec());
        } else if (namedType === NamedTypeIndex.Enum) {
          namedTypes.push(this.parseEnumTypeSpec());
        } else {
          throw new Error(
            `Bad byte 0x${toHexString(namedType)} used for namedTypeSpec index ` +
              `should be either 0x${toHexString(NamedTypeIndex.Struct)} for a struct ` +
              `or 0x${toHexString(NamedTypeIndex.Enum)} for an enum`
          );
        }
      } else {
        namedTypes.push(this.parseStructTypeSpec());
      }
    }
    return namedTypes;
  }

  private parseStructTypeSpec(): StructTypeSpec {
    const name = this.bufferReader.readString();
    const fieldsSize = this.bufferReader.readI32();
    const fields: FieldAbi[] = [];
    for (let j = 0; j < fieldsSize; j++) {
      const fieldName = this.bufferReader.readString();
      const type = this.parseTypeSpec();
      fields.push({ name: fieldName, type });
    }
    return new StructTypeSpec(name, fields, DocumentationNamedType.empty());
  }

  private parseEnumTypeSpec(): EnumTypeSpec {
    const name = this.bufferReader.readString();
    const numVariants = this.bufferReader.readI32();
    const variants: EnumVariant[] = [];
    for (let i = 0; i < numVariants; i++) {
      const discriminant = this.bufferReader.readU8();
      const typeIndex = this.bufferReader.readU8();
      if (typeIndex !== TypeIndex.Named) {
        throw new Error(
          `Non named type ${TypeIndex[typeIndex] ?? "Unknown"} used as an enum variant, ` +
            "each variant should be a reference to a struct"
        );
      }
      const index = this.bufferReader.readU8();
      variants.push({ discriminant, def: { typeIndex, index } });
    }
    return new EnumTypeSpec(name, variants);
  }

  private parseTypeSpec(): TypeSpec {
    const typeIndex = this.bufferReader.readU8();

    if (!(typeIndex in TypeIndex)) {
      throw new Error(`Bad byte 0x${toHexString(typeIndex)} used for type`);
    }

    if (typeIndex === TypeIndex.Named) {
      const index = this.bufferReader.readU8();
      return { typeIndex, index };
    } else if (typeIndex === TypeIndex.Vec) {
      const valueType = this.parseTypeSpec();
      return { typeIndex, valueType };
    } else if (typeIndex === TypeIndex.Option) {
      const valueType = this.parseTypeSpec();
      return { typeIndex, valueType };
    } else if (typeIndex === TypeIndex.Map || typeIndex == TypeIndex.AvlTreeMap) {
      const keyType = this.parseTypeSpec();
      const valueType = this.parseTypeSpec();
      return { typeIndex, keyType, valueType };
    } else if (typeIndex === TypeIndex.Set) {
      const valueType = this.parseTypeSpec();
      return { typeIndex, valueType };
    } else if (typeIndex === TypeIndex.SizedByteArray) {
      const length = this.bufferReader.readU8();
      return { typeIndex, length };
    } else {
      return { typeIndex };
    }
  }

  private parseFnAbi(config: Configuration, defaultFnKind: FnKind): FnAbi {
    let fnKind;
    if (config.fnType === FunctionFormat.FnKind) {
      const kindId = this.bufferReader.readU8();
      fnKind = fromKindId(kindId);
      if (fnKind === undefined) {
        throw new Error(`Unsupported FnKind type 0x${toHexString(kindId)} specified`);
      }
    } else {
      fnKind = defaultFnKind;
    }

    const name = this.bufferReader.readString();
    let shortname;
    if (config.shortnameType === ShortnameType.leb128) {
      shortname = this.parseLeb128();
      if (shortname.length > 5) {
        throw new Error(
          "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5 bytes)"
        );
      }
    } else {
      if (fnKind.kindId === 1) {
        shortname = Buffer.from([0, 0, 0, 0]);
      } else {
        shortname = AbiParser.parseHashShortname(config.shortnameLength as number, name);
      }
    }

    const fnArguments = [];
    const argumentsSize = this.bufferReader.readI32();
    for (let i = 0; i < argumentsSize; i++) {
      const nameArg = this.bufferReader.readString();
      const argType = this.parseTypeSpec();
      fnArguments.push({ name: nameArg, type: argType });
    }
    if (fnKind === FnKinds.zkSecretInputWithExplicitType) {
      const secretNameArg = this.bufferReader.readString();
      const secretArgType = this.parseTypeSpec();
      return new FnAbi(fnKind, name, shortname, fnArguments, {
        name: secretNameArg,
        type: secretArgType,
      });
    }

    return new FnAbi(fnKind, name, shortname, fnArguments);
  }

  private static parseHashShortname(shortnameLength: number, name: string): Buffer {
    const nameBuffer = Buffer.from(name);
    const hash = hashBuffer(nameBuffer);
    return hash.slice(0, shortnameLength);
  }

  private parseLeb128(): Buffer {
    let byte: number;
    const bufferWriter = new BigEndianByteOutput();
    do {
      byte = this.bufferReader.readU8();
      bufferWriter.writeU8(byte);
    } while (byte >= 128);
    return bufferWriter.toBuffer();
  }

  private parseActions(config: Configuration): FnAbi[] {
    const actionList = [];
    const actionsSize = this.bufferReader.readI32();
    for (let i = 0; i < actionsSize; i++) {
      const action = this.parseFnAbi(config, FnKinds.action);
      actionList.push(action);
    }
    return actionList;
  }

  public static printModel(model: FileAbi): string {
    const printer = new RustSyntaxPrettyPrinter(model);
    return printer.printModel();
  }

  public static printFunction(model: FileAbi, fnAbi: FnAbi): string {
    const printer = new RustSyntaxPrettyPrinter(model);
    return printer.printFunction(fnAbi);
  }

  public static printStruct(model: FileAbi, struct: StructTypeSpec): string {
    const printer = new RustSyntaxPrettyPrinter(model);
    return printer.printStruct(struct);
  }
}
