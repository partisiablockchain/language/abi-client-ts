/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ConfigOption, FunctionFormat, NamedTypesFormat, ShortnameType } from "../types/Abi";
import { AbiVersion } from "../types/AbiVersion";

export class Configuration {
  public readonly shortnameType: ShortnameType;
  public readonly fnType: FunctionFormat;
  public readonly shortnameLength: number | null;
  public readonly namedTypesFormat: NamedTypesFormat;

  private static readonly supportedClientVersions: Map<string, ConfigOption> = new Map([
    [
      "1.0.0",
      {
        shortnameType: ShortnameType.hash,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "2.0.0",
      {
        shortnameType: ShortnameType.hash,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "3.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "3.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "4.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "4.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "5.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
      },
    ],
    [
      "5.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
      },
    ],
    [
      "5.2.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
      },
    ],
    [
      "5.3.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
      },
    ],
    [
      "5.4.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
      },
    ],
  ]);

  constructor(
    shortnameType: ShortnameType,
    fnType: FunctionFormat,
    shortnameLength: number | null,
    namedTypesFormat: NamedTypesFormat
  ) {
    this.shortnameType = shortnameType;
    this.fnType = fnType;
    this.shortnameLength = shortnameLength;
    this.namedTypesFormat = namedTypesFormat;
  }

  public static fromClientVersion(
    version: AbiVersion,
    shortnameLength: number | null
  ): Configuration {
    const options = Configuration.getOptionsOrThrow(version);
    return new Configuration(
      options.shortnameType,
      options.fnType,
      shortnameLength,
      options.namedTypesFormat
    );
  }

  public static getOptionsOrThrow(clientVersion: AbiVersion): ConfigOption {
    const options = this.supportedClientVersions.get(clientVersion.withZeroPatch().toString());
    if (options === undefined) {
      throw new Error(`Unsupported Version ${clientVersion} for Version Client.`);
    } else {
      return options;
    }
  }
}
