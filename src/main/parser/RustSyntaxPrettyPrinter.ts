/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  ArgumentAbi,
  EnumVariant,
  FieldAbi,
  FnKind,
  GenericTypeAbi,
  NamedTypeRef,
  NamedTypeSpec,
  TypeSpec,
} from "../types/Abi";
import { AbiVersion } from "../types/AbiVersion";
import { DocumentationFunction } from "../types/DocumentationFunction";
import { DocumentationNamedType } from "../types/DocumentationNamedType";
import { EnumTypeSpec } from "../types/EnumTypeSpec";
import { FileAbi } from "../types/FileAbi";
import { FnAbi } from "../types/FnAbi";
import { StructTypeSpec } from "../types/StructTypeSpec";
import { FnKinds } from "./FnKinds";
import { TypeSpecStringifier } from "./TypeSpecStringifier";

export class RustSyntaxPrettyPrinter {
  private readonly model;
  private readonly typeStringifier: TypeSpecStringifier;

  constructor(model: FileAbi) {
    this.model = model;
    this.typeStringifier = new TypeSpecStringifier(model.contract.namedTypes.map((t) => t.name));
  }

  printModel(): string {
    const builder: string[] = [];

    const headerBuilder = new DocBuilder("//! ", builder, this.typeStringifier);
    for (const line of this.model.description) {
      headerBuilder.appendLine(line);
    }

    if (this.model.description.length > 0) {
      headerBuilder.appendLine("\n\n");
    }

    // Build version information
    builder.push("// Version Binder: ");
    this.buildVersion(builder, this.model.versionBinder);
    builder.push("// Version Client: ");
    this.buildVersion(builder, this.model.versionClient);
    builder.push("\n");

    // Build list of struct definition
    const contract = this.model.contract;
    const namedTypesCopy = [...contract.namedTypes];
    namedTypesCopy.splice((contract.stateType as NamedTypeRef).index, 1);
    namedTypesCopy.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
    for (const namedTypeSpec of namedTypesCopy) {
      this.buildNamedDefinition(builder, namedTypeSpec);
    }
    this.buildStateDefinition(builder);
    const functions = [...contract.functions].sort((a, b) => {
      if (a.kind.kindId != b.kind.kindId) {
        return this.numberCompare(a.kind.kindId, b.kind.kindId);
      } else {
        return a.shortname.compare(b.shortname);
      }
    });
    for (const func of functions) {
      this.buildFunction(builder, func);
    }

    return builder.join("");
  }

  numberCompare(a: number, b: number): number {
    return a - b;
  }

  private buildVersion(builder: string[], version: AbiVersion) {
    builder.push(`${version.major}.${version.minor}.${version.patch}\n`);
  }

  printFunction(func: FnAbi): string {
    const builder: string[] = [];
    this.buildFunction(builder, func);
    return builder.join("");
  }

  printStruct(struct: StructTypeSpec): string {
    const builder: string[] = [];
    const stateStruct = this.model.contract.getStateStruct();
    if (struct === stateStruct) {
      this.buildStateDefinition(builder);
    } else {
      this.buildStructDefinition(builder, struct);
    }
    return builder.join("");
  }

  private readonly FN_KINDS_WITH_EXPLICIT_SHORTNAME = [
    FnKinds.action,
    FnKinds.callback,
    FnKinds.zkSecretInput,
    FnKinds.zkSecretInputWithExplicitType,
  ];

  private buildFunction(builder: string[], func: FnAbi) {
    this.buildDocumentationForFn(builder, func, func.doc);
    builder.push("#[");
    builder.push(this.fnKindToRustName(func.kind));

    if (this.FN_KINDS_WITH_EXPLICIT_SHORTNAME.includes(func.kind)) {
      builder.push(`(shortname = 0x${func.shortname.toString("hex")}`);
      const type = func.secretArgument?.type;
      // This implicitly means that it is secret input with explicity type
      if (type !== undefined) {
        builder.push(', secret_type = "');
        this.typeStringifier.stringify(builder, type);
        builder.push('"');
      }
      builder.push(")");
    }
    builder.push("]\n");

    builder.push(`pub fn ${func.name} (`);
    if (func.arguments.length > 0) {
      builder.push("\n");
    }
    for (const arg of func.arguments) {
      this.buildArgument(builder, arg);
    }
    builder.push(")\n");
  }

  private fnKindToRustName(kind: FnKind): string {
    const kindName = this.FN_KIND_MACRO_NAMES.get(kind.kindId);
    return kindName as string;
  }

  private readonly FN_KIND_MACRO_NAMES = new Map<number, string>([
    [FnKinds.init.kindId, "init"],
    [FnKinds.action.kindId, "action"],
    [FnKinds.callback.kindId, "callback"],
    [FnKinds.zkSecretInput.kindId, "zk_on_secret_input"],
    [FnKinds.zkVarInputted.kindId, "zk_on_variable_inputted"],
    [FnKinds.zkVarRejected.kindId, "zk_on_variable_rejected"],
    [FnKinds.zkComputeComplete.kindId, "zk_on_compute_complete"],
    [FnKinds.zkVarOpened.kindId, "zk_on_variable_opened"],
    [FnKinds.zkUserVarOpened.kindId, "zk_on_user_variables_opened"],
    [FnKinds.zkAttestationComplete.kindId, "zk_on_attestation_complete"],
    [FnKinds.zkSecretInputWithExplicitType.kindId, "zk_on_secret_input_with_explicit_type"],
    [FnKinds.zkOnExternalEvent.kindId, "zk_on_external_event"],
  ]);

  private buildDocumentationForStruct(
    builder: string[],
    structAbi: StructTypeSpec,
    doc: DocumentationNamedType
  ) {
    const docBuilder = new DocBuilder("/// ", builder, this.typeStringifier);
    docBuilder.appendParagraph(doc.description);
    docBuilder.appendFields(structAbi.fields, doc.fields);
  }

  private buildDocumentationForFn(builder: string[], fnAbi: FnAbi, doc: DocumentationFunction) {
    const docBuilder = new DocBuilder("/// ", builder, this.typeStringifier);
    docBuilder.appendParagraph(doc.description);
    docBuilder.appendArguments(fnAbi.arguments, doc.arguments);

    if (doc.returns != null) {
      docBuilder.appendReturns(doc.returns);
    }
  }

  private buildArgument(builder: string[], arg: ArgumentAbi) {
    builder.push(`    ${arg.name}: `);
    this.typeStringifier.stringify(builder, arg.type);
    builder.push(",\n");
  }

  private buildNamedDefinition(builder: string[], named: NamedTypeSpec) {
    if (named instanceof StructTypeSpec && !this.isEnumVariant(named as StructTypeSpec)) {
      this.buildStructDefinition(builder, named as StructTypeSpec);
    } else if (named instanceof EnumTypeSpec) {
      this.buildEnumDefinition(builder, named as EnumTypeSpec);
    }
  }

  private isEnumVariant(structTypeSpec: StructTypeSpec): boolean {
    const namedTypeSpecs = this.model.contract.namedTypes;
    for (const namedTypeSpec of namedTypeSpecs) {
      if (namedTypeSpec instanceof EnumTypeSpec) {
        const enumTypeSpec = namedTypeSpec as EnumTypeSpec;
        for (const variant of enumTypeSpec.variants) {
          if (this.model.contract.getNamedType(variant.def) === structTypeSpec) {
            return true;
          }
        }
      }
    }
    return false;
  }

  private buildEnumDefinition(builder: string[], enumTypeSpec: EnumTypeSpec) {
    builder.push("enum " + enumTypeSpec.name + " {\n");
    for (const variant of enumTypeSpec.variants) {
      this.buildEnumVariant(builder, variant);
    }
    builder.push("}\n");
  }

  private buildEnumVariant(builder: string[], variant: EnumVariant) {
    const namedTypeSpec = this.model.contract.getNamedType(variant.def) as StructTypeSpec;
    builder.push("    " + "#[discriminant(" + variant.discriminant + ")]\n");
    builder.push("    " + namedTypeSpec.name + " {");

    for (let i = 0; i < namedTypeSpec.fields.length; i++) {
      const field = namedTypeSpec.fields[i];
      builder.push(" " + field.name + ": ");
      this.typeStringifier.stringify(builder, field.type);
      if (i < namedTypeSpec.fields.length - 1) {
        builder.push(",");
      } else {
        builder.push(" ");
      }
    }
    builder.push("},\n");
  }

  private buildStateDefinition(builder: string[]) {
    const state = this.model.contract.getStateStruct();
    this.buildDocumentationForStruct(builder, state, state.doc);
    builder.push("#[state]\n");
    builder.push(`pub struct ${state.name} {\n`);

    for (const field of state.fields) {
      this.buildFields(builder, field);
    }
    builder.push("}\n");
  }

  private buildStructDefinition(builder: string[], struct: StructTypeSpec) {
    this.buildDocumentationForStruct(builder, struct, struct.doc);

    builder.push(`pub struct ${struct.name} {\n`);

    for (const field of struct.fields) {
      this.buildFields(builder, field);
    }
    builder.push("}\n");
  }

  private buildFields(builder: string[], field: FieldAbi) {
    builder.push(`    ${field.name}: `);
    this.typeStringifier.stringify(builder, field.type);
    builder.push(",\n");
  }
}

export class DocBuilder {
  private readonly prefix: string;
  private readonly builder: string[];
  private readonly typeSpecPrinter: TypeSpecStringifier;

  constructor(prefix: string, builder: string[], typeSpecPrinter: TypeSpecStringifier) {
    this.prefix = prefix;
    this.builder = builder;
    this.typeSpecPrinter = typeSpecPrinter;
  }

  appendParagraph(paragraph: string[]) {
    for (const line of paragraph) {
      this.appendLine(line);
    }

    if (paragraph.length > 0) {
      this.appendLine("");
    }
  }

  appendLine(line: string) {
    this.builder.push(this.prefix);
    this.builder.push(line);
    this.builder.push("\n");
  }

  appendArguments(args: ArgumentAbi[], descriptions: Map<string, string>) {
    this.appendGenericParamList("# Parameters", args, descriptions);
  }

  appendFields(fields: FieldAbi[], descriptions: Map<string, string>) {
    this.appendGenericParamList("### Fields", fields, descriptions);
  }

  appendGenericParamList(
    sectionHeader: string,
    params: Array<GenericTypeAbi<TypeSpec>>,
    paramDescriptions: Map<string, string>
  ) {
    if (this.countParamLines(params, paramDescriptions) > 0) {
      this.appendLine(sectionHeader);
      this.appendLine("");

      for (const arg of params) {
        const description = paramDescriptions.get(arg.name);
        if (description !== undefined) {
          this.appendArgOrField(arg.name, arg.type, description);
        }
      }
      this.appendLine("");
    }
  }

  countParamLines(
    params: Array<GenericTypeAbi<TypeSpec>>,
    descriptions: Map<string, string>
  ): number {
    let count = 0;
    for (const arg of params) {
      if (descriptions.get(arg.name) !== undefined) {
        count += 1;
      }
    }
    return count;
  }

  appendArgOrField(name: string, type: TypeSpec, description: string) {
    this.builder.push(...[this.prefix, "* `", name, "`: "]);

    this.builder.push("[`");
    this.typeSpecPrinter.stringify(this.builder, type);
    this.builder.push("`], ");

    this.builder.push(...[description, "\n"]);
  }

  appendReturns(returns: string) {
    this.appendLine("# Returns:");
    this.appendLine("");
    this.appendLine(returns);
    this.appendLine("");
  }

  build(): string {
    return this.builder.join("");
  }
}
