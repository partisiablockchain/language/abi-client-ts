/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { FnKind, NamedTypeRef, NamedTypeSpec, TypeSpec } from "../types/Abi";
import { FnAbi } from "../types/FnAbi";
import { StructTypeSpec } from "../types/StructTypeSpec";
import { FnKinds } from "./FnKinds";

export class ContractAbi {
  public readonly namedTypes: NamedTypeSpec[];
  public readonly functions: FnAbi[];
  public readonly stateType: TypeSpec;

  constructor(namedTypes: NamedTypeSpec[], functions: FnAbi[], stateType: TypeSpec) {
    this.namedTypes = namedTypes;
    this.functions = functions;
    this.stateType = stateType;
  }

  public init(): FnAbi | undefined {
    return this.functions.find((v) => v.kind.kindId === FnKinds.init.kindId);
  }

  public getFunctionByName(name: string): FnAbi | undefined {
    return this.functions.find((v) => v.name === name);
  }

  public getFunction(shortname: Buffer, kind: FnKind): FnAbi | undefined {
    return this.functions.find((v) => v.shortname.equals(shortname) && v.kind === kind);
  }

  public getStateStruct(): StructTypeSpec {
    return this.namedTypes[(this.stateType as NamedTypeRef).index] as StructTypeSpec;
  }

  public isZk(): boolean {
    return this.functions.some(
      (fn) =>
        fn.kind === FnKinds.zkComputeComplete ||
        fn.kind === FnKinds.zkSecretInput ||
        fn.kind === FnKinds.zkUserVarOpened ||
        fn.kind === FnKinds.zkVarInputted ||
        fn.kind === FnKinds.zkVarOpened ||
        fn.kind === FnKinds.zkVarRejected ||
        fn.kind === FnKinds.zkAttestationComplete ||
        fn.kind === FnKinds.zkOnExternalEvent
    );
  }

  public getNamedType(struct: NamedTypeRef): NamedTypeSpec;
  public getNamedType(name: string): NamedTypeSpec | undefined;
  public getNamedType(struct: NamedTypeRef | string): NamedTypeSpec | undefined {
    if (typeof struct === "string") {
      return this.namedTypes.find((namedType) => namedType.name === struct);
    } else {
      return this.namedTypes[struct.index];
    }
  }
}
