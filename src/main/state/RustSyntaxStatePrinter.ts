/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ScValue } from "../value/ScValue";
import { ScValueEnum } from "../value/ScValueEnum";
import { ScValueStruct } from "../value/ScValueStruct";
import { TypeIndex } from "../types/Abi";
import { ScValueVector } from "../value/ScValueVector";
import { ScValueSet } from "../value/ScValueSet";
import { ScValueOption } from "../value/ScValueOption";
import { ScValueMap } from "../value/ScValueMap";

export class RustSyntaxStatePrinter {
  public static printState(state: ScValueStruct): string {
    const builder: string[] = [];
    builder.push("#[state]\n");
    builder.push(state.name + " {\n");

    state.fieldsMap.forEach((value, fieldname) => {
      this.buildField(builder, fieldname, value, 1);
      builder.push(",\n");
    });
    builder.push("}");
    return builder.join("");
  }

  private static buildIndent(builder: string[], indentLevel: number) {
    builder.push("    ".repeat(indentLevel));
  }

  private static buildField(
    builder: string[],
    fieldName: string,
    stateValue: ScValue,
    indentLevel: number
  ) {
    this.buildIndent(builder, indentLevel);
    builder.push(fieldName + ": ");
    this.buildStateValue(builder, stateValue, indentLevel);
  }

  private static buildStateValue(builder: string[], stateValue: ScValue, indentLevel: number) {
    const typeIndex = stateValue.getType();

    if (typeIndex === TypeIndex.Named) {
      this.buildNamed(builder, stateValue, indentLevel);
    } else if (typeIndex === TypeIndex.Vec) {
      this.buildVec(builder, stateValue.vecValue(), indentLevel);
    } else if (typeIndex === TypeIndex.Option) {
      this.buildOption(builder, stateValue.optionValue(), indentLevel);
    } else if (typeIndex === TypeIndex.Map) {
      this.buildMap(builder, stateValue.mapValue(), indentLevel);
    } else if (typeIndex === TypeIndex.Set) {
      this.buildSet(builder, stateValue.setValue(), indentLevel);
    } else if (typeIndex === TypeIndex.SizedByteArray) {
      this.buildArray(builder, stateValue.sizedByteArrayValue());
    } else if (typeIndex === TypeIndex.bool) {
      builder.push("" + stateValue.boolValue());
    } else if (typeIndex === TypeIndex.Address) {
      builder.push('"');
      builder.push(stateValue.addressValue().value.toString("hex"));
      builder.push('"');
    } else if (typeIndex === TypeIndex.String) {
      builder.push('"');
      builder.push(stateValue.stringValue());
      builder.push('".to_string()');
    } else {
      builder.push(stateValue.asBN().toString());
      builder.push(TypeIndex[typeIndex]);
    }
  }

  private static buildNamed(builder: string[], value: ScValue, indentLevel: number) {
    if (value instanceof ScValueStruct) {
      this.buildStruct(builder, value.structValue(), indentLevel);
    } else {
      this.buildEnum(builder, value.enumValue(), indentLevel);
    }
  }

  private static buildEnum(builder: string[], enumValue: ScValueEnum, indentLevel: number) {
    this.buildStruct(builder, enumValue.item, indentLevel);
  }

  private static buildStruct(builder: string[], struct: ScValueStruct, indentLevel: number) {
    builder.push(struct.name);
    builder.push(" {");
    const ite = struct.fieldsMap.entries();
    let kv = ite.next();
    while (kv.done === false) {
      const [k, v] = kv.value;
      builder.push("\n");
      this.buildField(builder, k, v, indentLevel + 1);
      kv = ite.next();
      if (kv.done === false) {
        builder.push(",");
      }
    }
    builder.push("\n");
    this.buildIndent(builder, indentLevel);
    builder.push("}");
  }

  private static buildVec(builder: string[], vec: ScValueVector, indentLevel: number) {
    builder.push("vec![");
    const vecIndentLevel = indentLevel + 1;
    for (let i = 0; i < vec.size(); i++) {
      builder.push("\n");
      this.buildIndent(builder, vecIndentLevel);
      this.buildStateValue(builder, vec.get(i), vecIndentLevel);
      if (i !== vec.size() - 1) {
        builder.push(",");
      }
    }
    builder.push("\n");
    this.buildIndent(builder, indentLevel);
    builder.push("]");
  }

  private static buildSet(builder: string[], set: ScValueSet, indentLevel: number) {
    builder.push("BTreeSet::from([");
    const setIndentLevel = indentLevel + 1;
    for (let i = 0; i < set.size(); i++) {
      builder.push("\n");
      this.buildIndent(builder, setIndentLevel);
      this.buildStateValue(builder, set.get(i), setIndentLevel);

      if (i !== set.size() - 1) {
        builder.push(",");
      }
    }
    builder.push("\n");
    this.buildIndent(builder, indentLevel);
    builder.push("])");
  }

  private static buildArray(builder: string[], array: Buffer) {
    builder.push("[");
    for (let i = 0; i < array.length; i++) {
      builder.push("" + array[i]);
      if (i < array.length - 1) {
        builder.push(", ");
      }
    }
    builder.push("]");
  }

  private static buildOption(builder: string[], option: ScValueOption, indentLevel: number) {
    if (option.innerValue !== null) {
      builder.push("Some(");
      this.buildStateValue(builder, option.innerValue, indentLevel);
      builder.push(")");
    } else {
      builder.push("None");
    }
  }

  private static buildMap(builder: string[], map: ScValueMap, indentLevel: number) {
    builder.push("BTreeMap::from([");
    const ite = map.map.entries();
    let kv = ite.next();
    const mapIndentLevel = indentLevel + 1;
    while (kv.done === false) {
      const [k, v] = kv.value;
      builder.push("\n");
      this.buildIndent(builder, mapIndentLevel);
      builder.push("(");
      this.buildStateValue(builder, k, mapIndentLevel);
      builder.push(", ");
      this.buildStateValue(builder, v, mapIndentLevel);
      builder.push(")");
      kv = ite.next();
      if (kv.done === false) {
        builder.push(",");
      }
    }
    builder.push("\n");
    this.buildIndent(builder, indentLevel);
    builder.push("])");
  }
}
