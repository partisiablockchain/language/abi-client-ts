/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbstractBuilder } from "../builder/AbstractBuilder";
import { ContractAbi } from "../parser/ContractAbi";
import { StructProducer } from "../builder/StructProducer";
import { LittleEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { AbiOutputBytes } from "../abioutput/AbiOutputBytes";

export class StateBuilder extends AbstractBuilder {
  constructor(contractAbi: ContractAbi | null) {
    super(contractAbi, "", new StructProducer(contractAbi?.getStateStruct() ?? null, "root"));
  }

  public getBytes(): Buffer {
    const out = new AbiOutputBytes(new LittleEndianByteOutput());
    this.getAggregateProducer().write(out);
    return out.toBuffer();
  }
}
