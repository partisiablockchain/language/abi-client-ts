/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiOutput } from "./AbiOutput";
import { ByteOutput } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";

export class AbiOutputBytes implements AbiOutput {
  private readonly output: ByteOutput;

  public constructor(output: ByteOutput) {
    this.output = output;
  }

  writeBytes(values: Buffer) {
    this.output.writeBytes(values);
  }

  writeBoolean(value: boolean) {
    this.output.writeBoolean(value);
  }

  writeI8(value: number) {
    this.output.writeI8(value);
  }

  writeI16(value: number) {
    this.output.writeI16(value);
  }

  writeI32(value: number) {
    this.output.writeI32(value);
  }

  writeI64(value: BN) {
    this.output.writeI64(value);
  }

  writeSignedBN(value: BN, noBytes: number) {
    this.output.writeSignedBigInteger(value, noBytes);
  }

  writeU8(value: number) {
    this.output.writeU8(value);
  }

  writeU16(value: number) {
    this.output.writeU16(value);
  }

  writeU32(value: number) {
    this.output.writeU32(value);
  }

  writeU64(value: BN) {
    this.output.writeU64(value);
  }

  writeUnsignedBN(value: BN, noBytes: number) {
    this.output.writeUnsignedBigInteger(value, noBytes);
  }

  writeString(value: string) {
    this.output.writeString(value);
  }

  toBuffer(): Buffer {
    return this.output.toBuffer();
  }
}
