/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";

export interface AbiOutput {
  /**
   * Writes a byte array.
   *
   * @param values the byte array to write to the stream
   */
  writeBytes(values: Buffer): void;

  /**
   * Writes a boolean.
   *
   * @param value the boolean to write to the stream
   */
  writeBoolean(value: boolean): void;

  /**
   * Writes a signed 8-bit number.
   *
   * @param value the number to write to the stream
   */
  writeI8(value: number): void;

  /**
   * Writes a signed 16-bit number.
   *
   * @param value the number to write to the stream
   */
  writeI16(value: number): void;

  /**
   * Writes a signed 32-bit number.
   *
   * @param value the number to write to the stream
   */
  writeI32(value: number): void;

  /**
   * Writes a signed 64-bit number.
   *
   * @param value the BN to write to the stream
   */
  writeI64(value: BN): void;

  /**
   * Writes a signed BN.
   *
   * @param value the BN to write to the stream
   * @param noBytes the number of bytes to write
   */
  writeSignedBN(value: BN, noBytes: number): void;

  /**
   * Writes an unsigned 8-bit number.
   *
   * @param value the number to write to the stream
   */
  writeU8(value: number): void;

  /**
   * Writes an unsigned 16-bit number.
   *
   * @param value the number to write to the stream
   */
  writeU16(value: number): void;

  /**
   * Writes an unsigned 32-bit number.
   *
   * @param value the number to write to the stream
   */
  writeU32(value: number): void;

  /**
   * Writes an unsigned 64-bit number.
   *
   * @param value the BN to write to the stream
   */
  writeU64(value: BN): void;

  /**
   * Writes an unsigned BN.
   *
   * @param value the BN to write to the stream
   * @param noBytes the number of bytes to write
   */
  writeUnsignedBN(value: BN, noBytes: number): void;

  /**
   * Writes a string.
   *
   * @param value the string to write to the stream
   */
  writeString(value: string): void;
}
