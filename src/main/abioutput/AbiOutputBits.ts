/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiOutput } from "./AbiOutput";
import { BitOutput } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";

export class AbiOutputBits implements AbiOutput {
  private readonly output: BitOutput;

  public constructor(output: BitOutput) {
    this.output = output;
  }

  writeBoolean(value: boolean): void {
    this.output.writeBoolean(value);
  }

  writeBytes(values: Buffer): void {
    this.output.writeBytes(values);
  }

  writeI16(value: number): void {
    this.output.writeSignedNumber(value, 16);
  }

  writeI32(value: number): void {
    this.output.writeSignedNumber(value, 32);
  }

  writeI64(value: BN): void {
    this.output.writeSignedBN(value, 64);
  }

  writeI8(value: number): void {
    this.output.writeSignedNumber(value, 8);
  }

  writeSignedBN(value: BN, noBytes: number): void {
    this.output.writeSignedBN(value, noBytes * 8);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  writeString(value: string): void {
    throw new Error("Unsupported write string for a bit output");
  }

  writeU16(value: number): void {
    this.output.writeUnsignedNumber(value, 16);
  }

  writeU32(value: number): void {
    this.output.writeUnsignedNumber(value, 32);
  }

  writeU64(value: BN): void {
    this.output.writeUnsignedBN(value, 64);
  }

  writeU8(value: number): void {
    this.output.writeUnsignedNumber(value, 8);
  }

  writeUnsignedBN(value: BN, noBytes: number): void {
    this.output.writeUnsignedBN(value, noBytes * 8);
  }
}
