/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { FnKind, TypeIndex } from "../types/Abi";
import { FileAbi } from "../types/FileAbi";
import { ByteInput } from "@secata-public/bitmanipulation-ts";
import { RpcValueFn } from "./RpcValueFn";
import { RpcReader } from "./RpcReader";
import { FnKinds } from "../parser/FnKinds";
import { ScValueNumber } from "../value/ScValueNumber";
import { AbiParser } from "../parser/AbiParser";
import { REAL_BINDER_ABI } from "../RealBinderInvocationDeserialization";
import { ScValue } from "../value/ScValue";
import { ScValueHash } from "../value/ScValueHash";
import { ScValuePublicKey } from "../value/ScValuePublicKey";
import { ScValueVector } from "../value/ScValueVector";
import { ScValueSignature } from "../value/ScValueSignature";
import { toHexString } from "../util/NumberToHex";
import { FnAbi } from "../types/FnAbi";
import { ScValueEnum } from "../value/ScValueEnum";
import { ScValueStruct } from "../value/ScValueStruct";

export class RealBinderInvocationReader {
  private readonly fileAbi: FileAbi;

  private static readonly REAL_BINDER: FileAbi = new AbiParser(REAL_BINDER_ABI).parseAbi();

  public static readonly COMMIT_RESULT_VARIABLE = 0x00;
  public static readonly ON_CHAIN_OUTPUT = 0x01;
  public static readonly UNABLE_TO_CALCULATE = 0x02;
  public static readonly OPEN_MASKED_INPUT = 0x03;
  public static readonly ZERO_KNOWLEDGE_INPUT_OFF_CHAIN = 0x04;
  public static readonly ZERO_KNOWLEDGE_INPUT_ON_CHAIN = 0x05;
  public static readonly REJECT_INPUT = 0x06;
  public static readonly ZK_OPEN_INVOCATION = 0x09;
  public static readonly ADD_ATTESTATION_SIGNATURE = 0x0a;
  public static readonly GET_COMPUTATION_DEADLINE = 0x0b;
  public static readonly ON_COMPUTE_COMPLETE = 0x0c;
  public static readonly ON_VARIABLES_OPENED = 0x0d;
  public static readonly ON_ATTESTATION_COMPLETE = 0x0e;
  public static readonly ON_VARIABLE_INPUTTED = 0x0f;
  public static readonly ADD_BATCHES = 0x12;
  public static readonly EXTEND_ZK_COMPUTATION = 0x13;
  private readonly reader: RpcReader;
  private readonly input: ByteInput;

  constructor(input: ByteInput, fileAbi: FileAbi, reader: RpcReader) {
    this.input = input;
    this.fileAbi = fileAbi;
    this.reader = reader;
  }

  public readRpc(): RpcValueFn {
    const shortname = this.input.readU8();
    switch (shortname) {
      case RealBinderInvocationReader.COMMIT_RESULT_VARIABLE:
        return this.deserializeCommitResultVariable();
      case RealBinderInvocationReader.ON_CHAIN_OUTPUT:
        return this.deserializeOnChainOutput();
      case RealBinderInvocationReader.UNABLE_TO_CALCULATE:
        return this.deserializeUnableToCalculate();
      case RealBinderInvocationReader.OPEN_MASKED_INPUT:
        return this.deserializeOpenMaskedInput();
      case RealBinderInvocationReader.ZERO_KNOWLEDGE_INPUT_OFF_CHAIN:
        return this.deserializeZeroKnowledgeInputOffChain(true);
      case RealBinderInvocationReader.ZERO_KNOWLEDGE_INPUT_ON_CHAIN:
        return this.deserializeZeroKnowledgeInputOnChain(true);
      case RealBinderInvocationReader.REJECT_INPUT:
        return this.deserializeRejectInput();
      case RealBinderInvocationReader.ZK_OPEN_INVOCATION:
        return this.reader.readPublicInvocation();
      case RealBinderInvocationReader.ADD_ATTESTATION_SIGNATURE:
        return this.deserializeAddAttestationSignature();
      case RealBinderInvocationReader.GET_COMPUTATION_DEADLINE:
        return this.deserializeGetComputationDeadline();
      case RealBinderInvocationReader.ON_COMPUTE_COMPLETE:
        return this.deserializeOnComputeComplete();
      case RealBinderInvocationReader.ON_VARIABLES_OPENED:
        return this.deserializeOnVariablesOpened();
      case RealBinderInvocationReader.ON_ATTESTATION_COMPLETE:
        return this.deserializeOnAttestationComplete();
      case RealBinderInvocationReader.ON_VARIABLE_INPUTTED:
        return this.deserializeOnVariableInputted();
      case RealBinderInvocationReader.ADD_BATCHES:
        return this.deserializeAddBatches();
      case RealBinderInvocationReader.EXTEND_ZK_COMPUTATION:
        return this.deserializeExtendZkComputation();
      default:
        throw Error(`Invocation identifier 0x${toHexString(shortname)} is not supported.`);
    }
  }

  private deserializeCommitResultVariable(): RpcValueFn {
    const id = this.input.readI64();
    const list = this.readRemainingAsListOfBytes();
    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.COMMIT_RESULT_VARIABLE]),
      FnKinds.action
    );

    const invocationArguments: ScValue[] = [
      new ScValueNumber(TypeIndex.i64, id),
      new ScValueVector(list),
    ];
    return new RpcValueFn(func, invocationArguments);
  }

  private deserializeOnChainOutput(): RpcValueFn {
    const id = this.input.readI32();
    const list = this.readRemainingAsListOfBytes();
    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ON_CHAIN_OUTPUT]),
      FnKinds.action
    );

    const invocationArguments: ScValue[] = [
      new ScValueNumber(TypeIndex.i32, id),
      new ScValueVector(list),
    ];
    return new RpcValueFn(func, invocationArguments);
  }

  private deserializeUnableToCalculate(): RpcValueFn {
    const blocktime = new ScValueNumber(TypeIndex.i64, this.input.readI64());
    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.UNABLE_TO_CALCULATE]),
      FnKinds.action
    );

    const invocationArguments: ScValue[] = [blocktime];
    return new RpcValueFn(func, invocationArguments);
  }

  private deserializeOpenMaskedInput(): RpcValueFn {
    const id = this.input.readI32();
    const maskedValue = this.readRemainingAsListOfBytes();

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.OPEN_MASKED_INPUT]),
      FnKinds.action
    );

    const invocationArguments: ScValue[] = [
      new ScValueNumber(TypeIndex.i32, id),
      new ScValueVector(maskedValue),
    ];
    return new RpcValueFn(func, invocationArguments);
  }

  private deserializeZeroKnowledgeInputOffChain(onlyPublic: boolean): RpcValueFn {
    const nrOfInputs = this.input.readI32();
    const invocationArguments: ScValue[] = [];
    const bitlengths: ScValue[] = [];
    for (let i = 0; i < nrOfInputs; i++) {
      bitlengths.push(new ScValueNumber(TypeIndex.i32, this.input.readI32()));
    }
    invocationArguments.push(new ScValueVector(bitlengths));
    invocationArguments.push(new ScValueHash(this.input.readBytes(32)));
    invocationArguments.push(new ScValueHash(this.input.readBytes(32)));
    invocationArguments.push(new ScValueHash(this.input.readBytes(32)));
    invocationArguments.push(new ScValueHash(this.input.readBytes(32)));

    const action = this.getSecretInputPublicInvocation();
    if (onlyPublic) {
      return action;
    }

    return new RpcValueFn(action.fnAbi, invocationArguments.concat(action.arguments));
  }

  private deserializeZeroKnowledgeInputOnChain(onlyPublic: boolean): RpcValueFn {
    const nrOfInputs = this.input.readI32();
    const invocationArguments: ScValue[] = [];
    const bitLengths: ScValue[] = [];
    for (let i = 0; i < nrOfInputs; i++) {
      bitLengths.push(new ScValueNumber(TypeIndex.i32, this.input.readI32()));
    }
    invocationArguments.push(new ScValueVector(bitLengths));
    invocationArguments.push(new ScValuePublicKey(this.input.readBytes(33)));
    invocationArguments.push(ScValueVector.fromBytes(this.input.readBytes(this.input.readI32())));

    const action = this.getSecretInputPublicInvocation();
    if (onlyPublic) {
      return action;
    }
    return new RpcValueFn(action.fnAbi, invocationArguments.concat(action.arguments));
  }

  private getSecretInputPublicInvocation(): RpcValueFn {
    const publicInvocation = this.input.readRemaining();
    try {
      return new RpcReader(
        publicInvocation,
        this.fileAbi,
        FnKinds.zkSecretInputWithExplicitType
      ).readPublicInvocation();
    } catch (e) {
      return new RpcReader(
        publicInvocation,
        this.fileAbi,
        FnKinds.zkSecretInput
      ).readPublicInvocation();
    }
  }

  private deserializeRejectInput(): RpcValueFn {
    const id: ScValueNumber = new ScValueNumber(TypeIndex.i32, this.input.readI32());

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.REJECT_INPUT]),
      FnKinds.action
    );

    const invocationArguments: ScValue[] = [id];
    return new RpcValueFn(func, invocationArguments);
  }

  private deserializeAddAttestationSignature(): RpcValueFn {
    const id: ScValueNumber = new ScValueNumber(TypeIndex.i32, this.input.readI32());
    const signature: ScValueSignature = new ScValueSignature(this.input.readRemaining());

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ADD_ATTESTATION_SIGNATURE]),
      FnKinds.action
    );

    return new RpcValueFn(func, [id, signature]);
  }

  private deserializeGetComputationDeadline(): RpcValueFn {
    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.GET_COMPUTATION_DEADLINE]),
      FnKinds.action
    );

    return new RpcValueFn(func, []);
  }

  private deserializeOnComputeComplete(): RpcValueFn {
    const nrOfVariables = this.input.readI32();
    const variableIds: ScValue[] = [];
    for (let i = 0; i < nrOfVariables; i++) {
      variableIds.push(new ScValueNumber(TypeIndex.i32, this.input.readI32()));
    }

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ON_COMPUTE_COMPLETE]),
      FnKinds.action
    );

    return new RpcValueFn(func, [new ScValueVector(variableIds)]);
  }

  private deserializeOnVariablesOpened(): RpcValueFn {
    const numberOfIds = this.input.readI32();
    const variableIds: ScValue[] = [];
    for (let i = 0; i < numberOfIds; i++) {
      variableIds.push(new ScValueNumber(TypeIndex.i32, this.input.readI32()));
    }

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ON_VARIABLES_OPENED]),
      FnKinds.action
    );

    return new RpcValueFn(func, [new ScValueVector(variableIds)]);
  }

  private deserializeOnAttestationComplete(): RpcValueFn {
    const id = this.input.readI32();

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ON_ATTESTATION_COMPLETE]),
      FnKinds.action
    );
    return new RpcValueFn(func, [new ScValueNumber(TypeIndex.i32, id)]);
  }

  private deserializeOnVariableInputted(): RpcValueFn {
    const id = this.input.readI32();

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ON_VARIABLE_INPUTTED]),
      FnKinds.action
    );
    return new RpcValueFn(func, [new ScValueNumber(TypeIndex.i32, id)]);
  }

  private deserializeAddBatches(): RpcValueFn {
    const batchType = this.input.readU8();
    let enumStruct;
    if (batchType === 0) {
      enumStruct = new ScValueStruct("input_mask", new Map());
    } else {
      enumStruct = new ScValueStruct("triple", new Map());
    }

    const batchEnum = new ScValueEnum("BatchType", enumStruct);

    const batchId = new ScValueNumber(TypeIndex.i32, this.input.readI32());

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.ADD_BATCHES]),
      FnKinds.action
    );

    return new RpcValueFn(func, [batchEnum, batchId]);
  }

  private deserializeExtendZkComputation(): RpcValueFn {
    const msPerGasNumerator: ScValueNumber = new ScValueNumber(TypeIndex.u64, this.input.readU64());
    const msPerGasDenominator: ScValueNumber = new ScValueNumber(
      TypeIndex.u64,
      this.input.readU64()
    );
    const minExtension: ScValueNumber = new ScValueNumber(TypeIndex.u64, this.input.readU64());
    const durationInMillis: ScValueNumber = new ScValueNumber(TypeIndex.u64, this.input.readU64());

    const func = this.getRealBinderInvocation(
      Buffer.from([RealBinderInvocationReader.EXTEND_ZK_COMPUTATION]),
      FnKinds.action
    );

    return new RpcValueFn(func, [
      msPerGasNumerator,
      msPerGasDenominator,
      minExtension,
      durationInMillis,
    ]);
  }

  public readSecretInput() {
    switch (this.input.readU8()) {
      case RealBinderInvocationReader.ZERO_KNOWLEDGE_INPUT_ON_CHAIN:
        return this.deserializeZeroKnowledgeInputOnChain(false);
      case RealBinderInvocationReader.ZERO_KNOWLEDGE_INPUT_OFF_CHAIN:
        return this.deserializeZeroKnowledgeInputOffChain(false);
      default:
        throw new Error("Invocation was not a secret input invocation.");
    }
  }

  readRemainingAsListOfBytes() {
    const remaining = this.input.readRemaining();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    return Array.from(remaining).map((val, _) => {
      return new ScValueNumber(TypeIndex.u8, val);
    });
  }

  public getRealBinderInvocation(shortname: Buffer, kind: FnKind): FnAbi {
    const func = RealBinderInvocationReader.REAL_BINDER.contract.getFunction(shortname, kind);
    if (func !== undefined) {
      return func;
    }
    return new FnAbi(kind, "Unknown Invocation", shortname, []);
  }
}
