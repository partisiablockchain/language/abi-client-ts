/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbstractReader } from "../AbstractReader";
import { FnKind, ShortnameType } from "../types/Abi";
import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { FileAbi } from "../types/FileAbi";
import { ScValueMap } from "../value/ScValueMap";
import { ScValueSet } from "../value/ScValueSet";
import { RpcValueFn } from "./RpcValueFn";
import { RealBinderInvocationReader } from "./RealBinderInvocationReader";
import { ScValueAvlTreeMap } from "../value/ScValueAvlTreeMap";

export class RpcReader extends AbstractReader {
  private readonly fileAbi: FileAbi;
  private readonly kind: FnKind;

  constructor(input: BigEndianByteInput | Buffer, fileAbi: FileAbi, kind: FnKind) {
    let byteInput;
    if ("length" in input) {
      byteInput = new BigEndianByteInput(input);
    } else {
      byteInput = new BigEndianByteInput(input.readRemaining());
    }
    super(byteInput, fileAbi.contract.namedTypes);
    this.fileAbi = fileAbi;
    this.kind = kind;
  }

  public readRpc(): RpcValueFn {
    if (this.fileAbi.contract.isZk()) {
      const realBinderReader: RealBinderInvocationReader = new RealBinderInvocationReader(
        this.input,
        this.fileAbi,
        this
      );
      return realBinderReader.readRpc();
    } else {
      return this.readPublicInvocation();
    }
  }

  public readSecretInputRpc(): RpcValueFn {
    if (this.fileAbi.contract.isZk()) {
      const realBinderReader: RealBinderInvocationReader = new RealBinderInvocationReader(
        this.input,
        this.fileAbi,
        this
      );
      return realBinderReader.readSecretInput();
    } else {
      throw Error("Cannot read Secret input for non zk contracts");
    }
  }

  public readPublicInvocation() {
    const shortname = this.readShortname();
    const action = this.fileAbi.contract.getFunction(shortname, this.kind);
    if (action === undefined) {
      throw Error(`No action with shortname ${shortname.toString("hex")}`);
    }
    return new RpcValueFn(
      action,
      action.arguments.map((arg) => this.readGeneric(arg.type))
    );
  }

  private parseLeb128(): Buffer {
    const bufferWriter = new BigEndianByteOutput();
    for (let i = 0; i < 5; i++) {
      const currentByte = this.input.readU8();
      bufferWriter.writeU8(currentByte);
      if (currentByte < 128) {
        return bufferWriter.toBuffer();
      }
    }
    throw new Error(
      "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5 bytes)"
    );
  }

  protected readMap(): ScValueMap {
    throw new Error("Type Map is not supported in rpc");
  }

  protected readSet(): ScValueSet {
    throw new Error("Type Set is not supported in rpc");
  }

  protected readAvlTreeMap(): ScValueAvlTreeMap {
    throw new Error("Type AvlTreeMap is not supported in rpc");
  }

  private readShortname(): Buffer {
    let shortname;
    if (this.fileAbi.format().shortnameType === ShortnameType.leb128) {
      shortname = this.parseLeb128();
    } else {
      shortname = this.input.readBytes(this.fileAbi.format().shortnameLength as number);
    }
    return shortname;
  }
}
