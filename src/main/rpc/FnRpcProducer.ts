/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ContractAbi } from "../parser/ContractAbi";
import { FnKinds } from "../parser/FnKinds";
import { FnKind, TypeSpec } from "../types/Abi";
import { FnAbi } from "../types/FnAbi";
import { Producer } from "../types/Producer";
import { AggregateProducer } from "../types/AggregateProducer";
import { AbiOutput } from "../abioutput/AbiOutput";
import { RealBinderInvocationReader } from "./RealBinderInvocationReader";

export class FnRpcProducer implements AggregateProducer {
  private readonly elements: Producer[] = [];
  private readonly action: FnAbi | null;
  private readonly shortname: Buffer;
  private readonly contractAbi: ContractAbi | null;

  constructor(name: string | Buffer, contractAbi?: ContractAbi, kind?: FnKind) {
    this.contractAbi = contractAbi ?? null;
    if (typeof name === "string" && contractAbi !== undefined) {
      const action = contractAbi.getFunctionByName(name);
      if (action === undefined) {
        throw new Error(`Contract must contain action with name ${name}`);
      }
      this.action = action;
      this.shortname = action.shortname;
    } else {
      const shortname = name as Buffer;
      if (contractAbi === undefined || kind === undefined) {
        this.action = null;
        this.shortname = shortname;
      } else {
        const action = contractAbi.getFunction(shortname, kind);
        if (action === undefined) {
          throw new Error(
            `Contract must contain action with shortname ${shortname.toString("hex")} and kind ${
              kind.kindId
            }`
          );
        }
        this.action = action;
        this.shortname = action.shortname;
      }
    }
  }

  public getTypeSpecForElement(): TypeSpec | null {
    if (this.action == null || this.action.arguments.length === this.elements.length) {
      return null;
    }
    const nextIndex = this.elements.length;
    return this.action.arguments[nextIndex].type;
  }

  public addElement(argument: Producer) {
    if (this.action !== null && this.action.arguments.length === this.elements.length) {
      throw new Error("Cannot add more arguments than the action expects.");
    }
    this.elements.push(argument);
  }

  public write(out: AbiOutput): void {
    if (
      this.contractAbi !== null &&
      this.action !== null &&
      this.action.kind === FnKinds.action &&
      this.contractAbi.isZk()
    ) {
      out.writeU8(RealBinderInvocationReader.ZK_OPEN_INVOCATION);
    }
    out.writeBytes(this.shortname);
    for (const element of this.elements) {
      element.write(out);
    }
    this.validate();
  }

  private validate() {
    if (this.action !== null && this.elements.length !== this.action.arguments.length) {
      const missingArgument = this.action.arguments[this.elements.length];
      throw new Error(`Missing argument '${missingArgument.name}'`);
    }
  }

  getFieldName(): string {
    if (this.action !== null && this.action.arguments.length !== this.elements.length) {
      const nextIndex = this.elements.length;
      const nextArgument = this.action.arguments[nextIndex];
      return this.action.name + "/" + nextArgument.name;
    }
    return "";
  }
}
