/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ScValueHash } from "../value/ScValueHash";

export class Hash {
  private readonly val: Buffer;
  constructor(val: Buffer) {
    this.val = val;
  }

  public asString(): string {
    return this.val.toString("hex");
  }

  public asBuffer(): Buffer {
    return this.val;
  }

  public static fromString(val: string): Hash {
    return this.fromBuffer(Buffer.from(val, "hex"));
  }

  public static fromBuffer(val: Buffer): Hash {
    if (val.length != ScValueHash.HASH_LENGTH) {
      throw new Error(
        `Hash expects exactly ${ScValueHash.HASH_LENGTH} bytes, but found ${val.length}`
      );
    }
    return new Hash(val);
  }
}
