/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AbiVersion } from "./AbiVersion";

export interface Header {
  header: string;
  versionBinder: AbiVersion;
  versionClient: AbiVersion;
}

export type FieldAbi = GenericTypeAbi<TypeSpec>;

export interface FnKind {
  kindId: number;
  minAllowedPerContract: number;
  maxAllowedPerContract: number;
}

export type ArgumentAbi = GenericTypeAbi<TypeSpec>;

export interface GenericTypeAbi<T extends TypeSpec> {
  name: string;
  type: T;
}

export type TypeSpec = SimpleTypeSpec | CompositeTypeSpec | NamedTypeRef;

export type CompositeTypeSpec =
  | VecTypeSpec
  | MapTypeSpec
  | SetTypeSpec
  | SizedByteArrayTypeSpec
  | OptionTypeSpec
  | AvlTreeMapTypeSpec;

export interface NamedTypeRef {
  typeIndex: TypeIndex.Named;
  index: number;
}

export class NamedTypeSpec {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export interface EnumVariant {
  discriminant: number;
  def: NamedTypeRef;
}

export enum ShortnameType {
  leb128,
  hash,
}

export enum FunctionFormat {
  InitSeparately,
  FnKind,
}

export enum NamedTypesFormat {
  OnlyStructs = 1,
  StructsAndEnum = 2,
}

export interface ConfigOption {
  shortnameType: ShortnameType;
  fnType: FunctionFormat;
  namedTypesFormat: NamedTypesFormat;
}

export enum NamedTypeIndex {
  Struct = 0x01,
  Enum = 0x02,
}

export enum TypeIndex {
  Named = 0x00,
  u8 = 0x01,
  u16 = 0x02,
  u32 = 0x03,
  u64 = 0x04,
  u128 = 0x05,
  i8 = 0x06,
  i16 = 0x07,
  i32 = 0x08,
  i64 = 0x09,
  i128 = 0x0a,
  String = 0x0b,
  bool = 0x0c,
  Address = 0x0d,
  Vec = 0x0e,
  Map = 0x0f,
  Set = 0x10,
  SizedByteArray = 0x11,
  Option = 0x12,
  Hash = 0x13,
  PublicKey = 0x14,
  Signature = 0x15,
  BlsPublicKey = 0x16,
  BlsSignature = 0x17,
  u256 = 0x18,
  AvlTreeMap = 0x19,
}

export type NumericTypeIndex = NumericTypeSpec["typeIndex"];

export type NumericTypeSpec =
  | U8TypeSpec
  | U16TypeSpec
  | U32TypeSpec
  | U64TypeSpec
  | U128TypeSpec
  | U256TypeSpec
  | I8TypeSpec
  | I16TypeSpec
  | I32TypeSpec
  | I64TypeSpec
  | I128TypeSpec;

export interface U8TypeSpec {
  typeIndex: TypeIndex.u8;
}

export interface U16TypeSpec {
  typeIndex: TypeIndex.u16;
}

export interface U32TypeSpec {
  typeIndex: TypeIndex.u32;
}

export interface U64TypeSpec {
  typeIndex: TypeIndex.u64;
}

export interface U128TypeSpec {
  typeIndex: TypeIndex.u128;
}

export interface U256TypeSpec {
  typeIndex: TypeIndex.u256;
}

export interface I8TypeSpec {
  typeIndex: TypeIndex.i8;
}

export interface I16TypeSpec {
  typeIndex: TypeIndex.i16;
}

export interface I32TypeSpec {
  typeIndex: TypeIndex.i32;
}

export interface I64TypeSpec {
  typeIndex: TypeIndex.i64;
}

export interface I128TypeSpec {
  typeIndex: TypeIndex.i128;
}

export type SimpleTypeIndex = SimpleTypeSpec["typeIndex"];

export type SimpleTypeSpec =
  | NumericTypeSpec
  | StringTypeSpec
  | BoolTypeSpec
  | AddressTypeSpec
  | HashTypeSpec
  | PublicKeyTypeSpec
  | SignatureTypeSpec
  | BlsPublicKeyTypeSpec
  | BlsSignatureTypeSpec;

export interface BoolTypeSpec {
  typeIndex: TypeIndex.bool;
}

export interface StringTypeSpec {
  typeIndex: TypeIndex.String;
}

export interface AddressTypeSpec {
  typeIndex: TypeIndex.Address;
}

export interface HashTypeSpec {
  typeIndex: TypeIndex.Hash;
}

export interface PublicKeyTypeSpec {
  typeIndex: TypeIndex.PublicKey;
}

export interface SignatureTypeSpec {
  typeIndex: TypeIndex.Signature;
}

export interface BlsPublicKeyTypeSpec {
  typeIndex: TypeIndex.BlsPublicKey;
}

export interface BlsSignatureTypeSpec {
  typeIndex: TypeIndex.BlsSignature;
}

export interface OptionTypeSpec {
  typeIndex: TypeIndex.Option;
  valueType: TypeSpec;
}

export interface VecTypeSpec {
  typeIndex: TypeIndex.Vec;
  valueType: TypeSpec;
}

export interface MapTypeSpec {
  typeIndex: TypeIndex.Map;
  keyType: TypeSpec;
  valueType: TypeSpec;
}

export interface SetTypeSpec {
  typeIndex: TypeIndex.Set;
  valueType: TypeSpec;
}

export interface SizedByteArrayTypeSpec {
  typeIndex: TypeIndex.SizedByteArray;
  length: number;
}

export interface AvlTreeMapTypeSpec {
  typeIndex: TypeIndex.AvlTreeMap;
  keyType: TypeSpec;
  valueType: TypeSpec;
}
