/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ArgumentAbi, FnKind } from "./Abi";
import { DocumentationFunction } from "./DocumentationFunction";

export class FnAbi {
  readonly kind: FnKind;
  readonly name: string;
  readonly shortname: Buffer;
  readonly arguments: ArgumentAbi[];
  readonly secretArgument: ArgumentAbi | undefined;
  readonly doc: DocumentationFunction;

  constructor(
    kind: FnKind,
    name: string,
    shortname: Buffer,
    args: ArgumentAbi[],
    secretArgument?: ArgumentAbi | null,
    doc?: DocumentationFunction | null
  ) {
    this.kind = kind;
    this.name = name;
    this.shortname = shortname;
    this.arguments = args;
    this.secretArgument = secretArgument ?? undefined;
    this.doc = doc ?? DocumentationFunction.empty();
  }
}
